package org.kavlihumanproject.fitnessapp.datamodel;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.kavlihumanproject.fitnessapp.util.TimeUtil.ONE_HOUR;
import static org.kavlihumanproject.fitnessapp.util.testing.TestingUtil.createTrackingSession;

public class TrackingSessionTest {



    @Test
    public void startSession() throws Exception {
        Random rand = new Random();
        for (int i  =  5; i < 10000; i += rand.nextInt(350)) {
            TrackingSession session = createTrackingSession(i, 0, 0, 0);
            verifyDistance(session, i);
            int duration = rand.nextInt((int)(5 * ONE_HOUR));
            session = createTrackingSession(i, 0, 0, duration);
            verifyDuration(session, ((double) duration) / ONE_HOUR );
        }
    }

    void verifyDistance(TrackingSession session, double expectedDistance){
        session.startSession()
                .last()
                .doOnNext((dist) -> assertTrue(Math.abs(expectedDistance - dist) < 1))
                .subscribe(this::p, this::failTest);
    }

    void verifyDuration(TrackingSession session, double expectedDuration){
        session.startSession()
                .last()
                .doOnNext((dist) -> {
                    double duration = session
                            .getSessionData()
                            .getDuration();

                    duration /= ONE_HOUR;
                    double diff = Math.abs(expectedDuration - duration);
                    assertTrue(diff < .01); // difference less than a minute
                })
                .subscribe(this::p, this::failTest);
    }

    @Test
    public void milestones() throws Exception {
        TrackingSession session1 = createTrackingSession(1001, 0, 0, 0);
        testMilestone(session1, 1000, 1, false);

        TrackingSession session2 = createTrackingSession(1001, 3500, 0, 0);
        testMilestone(session2, 4000, 1, false);

        TrackingSession session3 = createTrackingSession(5981, 3500, 0, 0);
        testMilestone(session3, 9000, 6, false);

        TrackingSession session4 = createTrackingSession(581, 0, 0, 0);
        testMilestone(session4, 0, 0, false);
    }

    int milestoneCount = 0;
    private synchronized void testMilestone(TrackingSession session, int expectedMilestone,
                                            int expectedMilestoneCount, boolean print) throws Exception{
        milestoneCount = 0;
        session.startSession()
                .doOnNext((d) -> {
                    if (session.didReachMilestone()){
                        milestoneCount++;
                    }
                })
                .doOnCompleted(() -> {
                    if (print) {
                        p("current milestone " + session.getCurrentMilestone());
                        p("milestone count " + milestoneCount);
                    }
                })
                .doOnCompleted(() -> assertTrue(session.getCurrentMilestone() == expectedMilestone))
                .doOnCompleted(() -> assertTrue(milestoneCount == expectedMilestoneCount))
                .subscribe(this::p, this::failTest);
    }

    @Test
    public void getSessionStats() throws Exception {
        Random random = new Random();
        for (int i = 0; i < 25; i++) {
            int sessionDist = random.nextInt(10000);
            int startingDist = random.nextInt(10000);
            TrackingSession session = createTrackingSession(sessionDist, startingDist, 0, 0);
            session.startSession()
                    .doOnCompleted(() -> {
                       assertTrue(Math.abs(session.getUpdatedStats().getDistance() - (sessionDist + startingDist)) < 1);
                    }).subscribe(this::p, this::failTest);
        }
    }


    void failTest(Throwable t) {
        fail(t.getLocalizedMessage());
    }

    void p(Object o){
        //System.out.println(o);
    }
}