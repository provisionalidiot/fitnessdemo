package org.kavlihumanproject.fitnessapp.tasks;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;

import rx.schedulers.Schedulers;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.kavlihumanproject.fitnessapp.datamodel.impl.ActivityFilter.fillMockIntent;


@RunWith(AndroidJUnit4.class)
public class ActivityTrackerServiceTest {

    private static final int WALKING = 7;
    private static final int STILL = 3;

    @Rule
    public final ServiceTestRule testRule = new ServiceTestRule();

    @Before
    public void setUp() throws Exception {
        TrackingSessionBuilder.setIsTest(true);
    }

    private Intent buildTrackingIntent(int activity){
        Intent activityRecognized = new Intent(InstrumentationRegistry.getTargetContext(),
                ActivityTrackerService.class);
        activityRecognized.putExtra(ActivityTrackerService.EXTRA_USER_ID, 0L);
        return fillMockIntent(activityRecognized, activity);
    }

    private Intent buildBindingIntent(){
         return new Intent(InstrumentationRegistry.getTargetContext(),
                ActivityTrackerService.class);
    }

    @Test
    public void testSessionStarted() throws Exception {
        testRule.startService(buildTrackingIntent(WALKING));
        ActivityTrackerService.LiveStatsBinder binder
                = (ActivityTrackerService.LiveStatsBinder) testRule.bindService(buildBindingIntent());

        TrackingSession session = binder.getSession();

        assertNotNull(session);
        assertTrue(session.isActive());
    }


    boolean completed = false;
    @Test
    public void testSessionStopped() throws Exception {
        testRule.startService(buildTrackingIntent(WALKING));
        ActivityTrackerService.LiveStatsBinder binder
                = (ActivityTrackerService.LiveStatsBinder) testRule.bindService(buildBindingIntent());

        TrackingSession session = binder.getSession();
        binder.listenForUpdates()
                .doOnSubscribe(() -> {
                    try {
                        testRule.startService(buildTrackingIntent(STILL));
                    }catch (Exception e){
                        throw new RuntimeException(e);
                    }
                })
                .doOnCompleted(() -> {completed =  true;})
                .doOnTerminate(() -> {
                    System.out.println("here1");
                    assertFalse(session.isActive());
                    assertTrue(completed);
                })
                .subscribeOn(Schedulers.io())
                 .toBlocking()
                .subscribe();
    }

}