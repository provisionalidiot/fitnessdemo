package org.kavlihumanproject.fitnessapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import org.kavlihumanproject.fitnessapp.dagger.ApplicationScope;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LocalUserSharedPrefsCacheImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DatabaseModule;
import org.kavlihumanproject.fitnessapp.tasks.TrackerServiceComponent;
import org.kavlihumanproject.fitnessapp.ui.DailyStatsComponent;
import org.kavlihumanproject.fitnessapp.ui.LoginComponent;
import org.kavlihumanproject.fitnessapp.ui.MainScreenComponent;
import org.kavlihumanproject.fitnessapp.ui.SplashComponent;

import dagger.Module;
import dagger.Provides;

import static org.kavlihumanproject.fitnessapp.BootReceiverModule.*;

public class FitnessAppDemo extends Application {

    private Component appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerFitnessAppDemo_Component
                .builder()
                .databaseModule(new DatabaseModule(this))
                .applicationModule(new ApplicationModule(this))
                .build();
    }


    public Component getAppComponent() {
        return appComponent;
    }

    @dagger.Component(modules = {ApplicationModule.class, DatabaseModule.class})
    @ApplicationScope
    public interface Component {
        SplashComponent.Builder splashComponentBuilder();
        LoginComponent.Builder loginComponentBuilder();
        TrackerServiceComponent.Builder trackerServiceComponentBuilder();
        MainScreenComponent.Builder mainScreenComponentBuilder();
        BootReceiverComponent.Builder bootReceiverComponentBuilder();
    }

    @Module(
            subcomponents = {
                    SplashComponent.class,
                    LoginComponent.class,
                    DailyStatsComponent.class,
                    BootReceiverComponent.class
            }
    )
    public class ApplicationModule {
        private static final String USER_CACHE_NAME = "fitness_app_local_user_cache";
        private final Context context;

        ApplicationModule(Context context) {
            this.context = context.getApplicationContext();
        }

        @Provides
        @ApplicationScope
        SharedPreferences provideSharedPrefs(){
            return context.getSharedPreferences(USER_CACHE_NAME, Context.MODE_PRIVATE);
        }

        @Provides @ApplicationScope
        LocalUserCache provideUserCache(SharedPreferences prefs){
            return new LocalUserSharedPrefsCacheImpl(prefs);
        }

    }
}
