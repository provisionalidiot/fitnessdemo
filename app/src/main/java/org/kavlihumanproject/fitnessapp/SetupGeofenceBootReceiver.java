package org.kavlihumanproject.fitnessapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.kavlihumanproject.fitnessapp.datamodel.impl.GeofenceBootRegistrar;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.tasks.ReminderService;

import static org.kavlihumanproject.fitnessapp.BootReceiverModule.BootReceiverComponent;

public class SetupGeofenceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Setup geofence", "action " + intent.getAction());
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            ReminderService.cancelAlarm(context);
            BootReceiverComponent component = getBootReceiverComponent(context);
            GeofenceBootRegistrar registrar = component.getRegistrar();
            registrar.register()
                    .subscribe(success -> {
                        registrar.release();
                    },Throwable::printStackTrace);
        }
    }

    private BootReceiverComponent getBootReceiverComponent(Context context){
        FitnessAppDemo app = (FitnessAppDemo) context
                .getApplicationContext();

        return app.getAppComponent()
                .bootReceiverComponentBuilder()
                .playServicesModule(new GooglePlayServicesModule(context))
                .bootReceiverModule(new BootReceiverModule())
                .build();
    }
}
