package org.kavlihumanproject.fitnessapp.dagger;

import android.os.Bundle;

import org.kavlihumanproject.fitnessapp.FitnessAppDemo;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.tasks.ActivityTrackerService;
import org.kavlihumanproject.fitnessapp.tasks.TrackerServiceComponent;
import org.kavlihumanproject.fitnessapp.tasks.TrackerServiceModule;
import org.kavlihumanproject.fitnessapp.ui.DailyStatsComponent;
import org.kavlihumanproject.fitnessapp.ui.DailyStatsFragment;
import org.kavlihumanproject.fitnessapp.ui.DailyStatsModule;
import org.kavlihumanproject.fitnessapp.ui.LeaderBoardComponent;
import org.kavlihumanproject.fitnessapp.ui.LeaderboardFragment;
import org.kavlihumanproject.fitnessapp.ui.LeaderboardModule;
import org.kavlihumanproject.fitnessapp.ui.LoginComponent;
import org.kavlihumanproject.fitnessapp.ui.LoginScreenActivity;
import org.kavlihumanproject.fitnessapp.ui.LoginScreenModule;
import org.kavlihumanproject.fitnessapp.ui.MainActivity;
import org.kavlihumanproject.fitnessapp.ui.MainScreenComponent;
import org.kavlihumanproject.fitnessapp.ui.MainScreenModule;
import org.kavlihumanproject.fitnessapp.ui.SplashActivity;
import org.kavlihumanproject.fitnessapp.ui.SplashComponent;
import org.kavlihumanproject.fitnessapp.ui.SplashScreenModule;

public class Injector {

    private Injector() {

    }

    public static void injectSplashActivity(SplashActivity activity) {
        FitnessAppDemo app = (FitnessAppDemo) activity
                .getApplication();

        SplashComponent splashComponent = app
                .getAppComponent()
                .splashComponentBuilder()
                .setPlayServicesModule(new GooglePlayServicesModule(activity))
                .setSplashScreenModule(new SplashScreenModule(activity))
                .build();
        splashComponent.inject(activity);
    }

    public static void injectLoginPage(LoginScreenActivity activity, Bundle saved) {
        FitnessAppDemo app = (FitnessAppDemo) activity
                .getApplication();

        LoginComponent loginComponent = app
                .getAppComponent()
                .loginComponentBuilder()
                .googlePlayServicesModule(new GooglePlayServicesModule(activity))
                .loginScreenModule(new LoginScreenModule(activity, saved))
                .build();

        loginComponent.inject(activity);
    }

    public static void injectMainScreen(MainActivity activity) {
        FitnessAppDemo app = (FitnessAppDemo) activity
                .getApplication();

        MainScreenComponent mainScreenComponent = app
                .getAppComponent()
                .mainScreenComponentBuilder()
                .mainScreenModule(new MainScreenModule(activity))
                .build();

        activity.setComponent(mainScreenComponent);
    }

    public static void injectDailyStatsFragment(DailyStatsFragment fragment) {
        MainActivity activity = (MainActivity) fragment.getActivity();
        DailyStatsComponent statsComponent = activity
                .getComponent()
                .dailyStatsComponentBuilder()
                .playServicesModule(new GooglePlayServicesModule(activity))
                .dailyStatsModule(new DailyStatsModule(activity, fragment.getModel()))
                .build();
        statsComponent.inject(fragment);
    }


    public static void injectLeaderboardFragment(LeaderboardFragment fragment) {
        MainActivity activity = (MainActivity) fragment.getActivity();
        LeaderBoardComponent leaderBoardComponent = activity
                .getComponent()
                .leaderboardComponentBuilder()
                .module(new LeaderboardModule(fragment.getModel()))
                .build();
        leaderBoardComponent.inject(fragment);
    }

    public static void injectTrackingSevice(ActivityTrackerService service) {
        FitnessAppDemo app = (FitnessAppDemo) service
                .getApplication();

        TrackerServiceComponent serviceComponent = app.getAppComponent()
                .trackerServiceComponentBuilder()
                .playServicesModule(new GooglePlayServicesModule(service))
                .trackerServiceModule(new TrackerServiceModule(service))
                .build();

        serviceComponent.inject(service);
    }
}
