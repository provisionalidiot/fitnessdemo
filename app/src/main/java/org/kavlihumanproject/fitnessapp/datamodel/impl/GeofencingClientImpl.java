package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.tasks.ReminderService;
import org.kavlihumanproject.fitnessapp.util.Permissions;

import rx.Observable;
import rx.schedulers.Schedulers;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class GeofencingClientImpl implements GeofencingClient {

    private static final String HANDLER_NAME = "GEOFENCING_HANDLER";
    private final GoogleApiProvider provider;
    private final LocationTracker tracker;

    public GeofencingClientImpl(GoogleApiProvider provider, LocationTracker tracker) {
        this.provider = provider;
        this.tracker = tracker;
    }

    @Override
    public Observable<LocationData> getCurrentLocation() {
        return tracker.startTracking()
                .take(1)
                .doOnNext((data) -> tracker.stopTracking());
    }

    @Override
    public Observable<Boolean> registerGeofence(GeoFence fence) {
        return provider.connect()
                .filter(GoogleApiClient::isConnected)
                .flatMap(client -> {
                    Context context = client.getContext();
                    if (Permissions.hasLocation(context)) {
                        unregister(client);
                        Status status = register(client, fence);
                        if (!status.isSuccess()){
                            return Observable
                                    .error(new GoogleApiException(status));
                        }
                        return Observable.just(true);
                    }
                    return Observable.just(false);
                });
    }

    @Override
    public Observable<Boolean> unregisterGeofence() {
        return provider.connect()
                .filter(GoogleApiClient::isConnected)
                .map(this::unregister)
                .subscribeOn(Schedulers.io());
    }

    boolean unregister(GoogleApiClient client){
        PendingIntent pi = buildPendingIntent(client.getContext());
        ReminderService.cancelAlarm(client.getContext());
        return LocationServices.GeofencingApi
                .removeGeofences(client, pi)
                .await()
                .isSuccess();
    }

    @Override
    public void release() {
        tracker.release();
    }

    Status register(GoogleApiClient client, GeofencingClient.GeoFence fence) throws SecurityException {
        GeofencingRequest request = newGeofenceRequest(fence.getKey(),
                fence.getLat(), fence.getLng());
        Context context = client.getContext();
        return LocationServices.GeofencingApi
                    .addGeofences(client, request,
                            buildPendingIntent(context))
                    .await();
    }

    PendingIntent buildPendingIntent(Context context){
        Intent startService = new Intent(context, ReminderService.class);
        startService.setAction(GEOFENCE_TRIGGER_ACTION);
        return PendingIntent
                .getService(context, 0, startService, FLAG_CANCEL_CURRENT);
    }

    GeofencingRequest newGeofenceRequest(String key, double lat, double lng){
        Geofence geofence = new Geofence.Builder()
                .setRequestId(key)
                .setCircularRegion(lat, lng, 200)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT |
                        Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(15 * 60 * 1000) // Fifteen minutes
                .build();

        return new GeofencingRequest.Builder()
                .addGeofence(geofence)
                .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_DWELL)
                .build();
    }
}
