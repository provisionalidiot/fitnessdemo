package org.kavlihumanproject.fitnessapp.datamodel.impl;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.datamodel.AppException;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.LoginPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.model.LoginScreenModel;
import org.kavlihumanproject.fitnessapp.util.StringsUtil;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class LoginPresenterImpl implements LoginPresenter {

    final private LoginScreenModel model;
    final private LocalUserProvider provider;
    final private GeofencingClient client;
    final private ScreenNavigator navigator;
    final private LocalUserCache cache;

    private Subscription subscription;

    public LoginPresenterImpl(LoginScreenModel model, LocalUserProvider provider,
                              GeofencingClient client, ScreenNavigator navigator, LocalUserCache cache) {
        this.model = model;
        this.provider = provider;
        this.client = client;
        this.navigator = navigator;
        this.cache = cache;
    }

    @Override
    public void imNewButtonClicked() {
        model.setShowConfirm(true);
        model.showErrorMessage(R.string.empty_space);
    }

    @Override
    public void cancelButtonClicked() {
        model.setShowConfirm(false);
        model.clearConfirm();
        model.showErrorMessage(R.string.empty_space);
    }

    @Override
    public void login() {
        if (validateLoginData()){
            model.setShowLoading(true);
            subscription = provider
                    .verifyLogin(model.getUserName(), model.getPasswd())
                    .flatMap(this::setupGeofence)
                    .flatMap(cache::setActiveUser)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onSuccess, this::onError);
        }
    }

    Observable<LocalUser> setupGeofence(LocalUser user){
        if (user.hasOffice()){
            return Observable.just(user.getOfficeLocation())
                    .map(GeofencingClient::convertToGeofence)
                    .flatMap(client::registerGeofence)
                    .flatMap(success -> success
                            ? client.getCurrentLocation()// Geofence tends not to register until location data is available, likely to conserve power. So force it,
                            : Observable.just(false)
                    )
                    .map(success -> user);
        }
        return Observable.just(user);
    }

    @Override
    public void createNewUser() {
        if (validateNewUserData()){
            model.setShowLoading(true);
            subscription = provider
                    .createNewUser(model.getUserName(), model.getPasswd())
                    .flatMap(cache::setActiveUser)
                    .subscribe(this::onSuccess, this::onError);
        }
    }

    @Override
    public void cancel() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        model.setShowLoading(false);
        navigator.release();
    }

    private boolean validateNewUserData(){
        if (!validateLoginData())
            return false;

        String confirm = model.getConfrm();
        String password = model.getPasswd();
        if (!password.equals(confirm)){
            model.showErrorMessage(R.string.error_password_mismatch);
            return false;
        }

        return true;
    }

    private boolean validateLoginData(){
        String userName = model.getUserName();
        if (StringsUtil.isEmpty(userName)){
            model.showErrorMessage(R.string.error_no_username);
            return false;
        }

        String password = model.getPasswd();
        if (StringsUtil.isEmpty(password)){
            model.showErrorMessage(R.string.error_no_password);
            return false;
        }

        return true;
    }

    void onSuccess(LocalUser value) {
        model.setShowLoading(false);
        if (!navigator.isReleased()) {
            navigator.relaunchSplash();
        }
    }

    void onError(Throwable e) {
        model.setShowLoading(false);
        if (e instanceof AppException){
            AppException exception = (AppException) e;
            model.showErrorMessage(exception.getErrorMessage());
        }else {
            throw new RuntimeException(e);
        }
    }
}
