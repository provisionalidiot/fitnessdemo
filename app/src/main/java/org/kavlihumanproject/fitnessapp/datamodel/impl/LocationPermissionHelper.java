package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.Activity;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LocationPermissionHelper extends PermissionHelperImpl {
    public LocationPermissionHelper(Activity activity) {
        super(activity, ACCESS_FINE_LOCATION);
    }
}
