package org.kavlihumanproject.fitnessapp.datamodel.impl;


import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.ui.model.DailyStatsModel;

import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
public class DailyStatsPresenter {
    private final DailyStatsModel model;
    private final DailyStatsProvider provider;
    private final GeofencingClient client;
    private final LocalUserCache cache;
    private final FitActivityTracker tracker;
    private final ScreenNavigator navigator;
    private final UserTableApi userTableApi;
    private final Scheduler dbScheduler;

    private Subscription subscription;

    public DailyStatsPresenter(DailyStatsModel model, DailyStatsProvider provider, GeofencingClient client,
                               LocalUserCache cache, FitActivityTracker tracker, ScreenNavigator navigator,
                               UserTableApi userTableApi, Scheduler dbScheduler) {
        this.model = model;
        this.provider = provider;
        this.client = client;
        this.cache = cache;
        this.tracker = tracker;
        this.navigator = navigator;
        this.userTableApi = userTableApi;
        this.dbScheduler = dbScheduler;
    }

    public void getRecentStats(){
        model.setShowLoading(true);
        LocalUser user = model.getUser();
        subscription = provider
                .getRecentStats(user.getId())
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::replace, this::onError);
    }

     void onError(Throwable e) {
        e.printStackTrace();
         model.setShowLoading(false);
         if (e instanceof GoogleApiException){
             GoogleApiException except = (GoogleApiException) e;
             if (except.isResolvable()){
                navigator.handle(except);
                return;
             }

             model.setErrorCode(except.getErrorCode());
         }
     }

    void replace(List<DailyStat> stats){
        model.setShowLoading(false);
        model.replaceStats(stats);
    }

    void cancelSubscription(){
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }

    public void release(){
        cancelSubscription();
        client.release();
    }

    public void setLocationAsOffice() {
            model.setShowLoading(true);
            subscription = client.getCurrentLocation()
                    .compose(this::setupGeoFence)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success -> {
                        model.setShowLoading(false);
                        cancelSubscription();
                    }, this::onError);
    }

    Observable<Boolean> setupGeoFence(Observable<LocationData> observable){
        return observable
                .map(GeofencingClient::convertToGeofence)
                .flatMap(geofence -> client.registerGeofence(geofence)
                        .observeOn(dbScheduler)
                        .doOnNext(success -> {
                            if (success){
                                long userId = model.getUser().getId();
                                userTableApi.updateOfficeLocation(userId,
                                        geofence.getLat(), geofence.getLng());
                            }
                        }));
    }

    public void logout() {
        subscription = cache.clearCache()
                .flatMap(obj -> client.unregisterGeofence())
                .flatMap(obj -> tracker.stopListening())
                .subscribe((success) -> {
                    navigator.relaunchSplash();
                }, this::onError);
    }
}
