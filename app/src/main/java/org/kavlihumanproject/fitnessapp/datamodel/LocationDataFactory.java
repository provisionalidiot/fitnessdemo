package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

public interface LocationDataFactory {
    LocationData create(double lat, double lng, long timestamp);
}
