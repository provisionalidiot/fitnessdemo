package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.util.Optional;

import rx.Observable;

public interface LocalUserCache {
    Observable<Optional<LocalUser>> getActiveUser();
    Observable<LocalUser> setActiveUser(LocalUser user);
    Observable<Boolean> clearCache();
}
