package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.database.Cursor;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.ui.model.DailyStatsItem;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import static org.kavlihumanproject.fitnessapp.util.TimeUtil.getToday;
import static org.kavlihumanproject.fitnessapp.util.TimeUtil.getTomorrow;

public class DailyStatsProvider {

    private static final int MAX_COUNT = 5000;                                                                                                                                           ;
    private final DailyStatsApi dailyStatsApi;
    private final Scheduler scheduler;
    private final SessionDataProvider sessionsProvider;

    @Inject
    public DailyStatsProvider(DailyStatsApi statsApi, SessionDataProvider sessionsProvider, Scheduler scheduler) {
        this.dailyStatsApi = statsApi;
        this.sessionsProvider = sessionsProvider;
        this.scheduler = scheduler;
    }

    public Observable<DailyStat> getRecentStats(long userId){
        return getStats(userId, System.currentTimeMillis());
    }

    public Observable<DailyStat> getStats(long userId, long before){
        return Observable.defer(() -> {
            Cursor cursor = dailyStatsApi
                    .getDailyStatistics(userId, before, MAX_COUNT);
            return Observable.create(
                    new CursorOnSubscribe<>(cursor, DailyStatsProvider::newDailyStat))
                    .onBackpressureBuffer();
        }).subscribeOn(scheduler);
    }

    public Observable<List<DailyStat>> recalculateDailyStats(long userId, long createdDate) {
        return Observable.just(userId)
                .map(dailyStatsApi::deleteStatsForUser)
                .subscribeOn(scheduler)
                .flatMap(success -> {
                    if (!success){
                        return Observable
                                .error(new RuntimeException("could not delete stats"));
                    }

                    List<Observable<DailyStat>> stats = new ArrayList<>();
                    long endDate = TimeUtil.getToday(createdDate);
                    for (long date = TimeUtil.getToday(); date >= endDate;
                         date = TimeUtil.getYesterday(date)) {
                        stats.add(calcDailyStat(userId, date));
                    }

                    return Observable.merge(stats);
                }).toList();
    }

    public Observable<DailyStat> calcDailyStat(long userId, long date){
            return getDailyStatId(userId, date)
                    .flatMap((id) ->
                            getSessions(userId, date)
                                .reduce(newDailyStat(id, date), DailyStat::plus)
                    )
                    .observeOn(scheduler)
                    .doOnNext((stat) -> {
                        dailyStatsApi.updateDailyStat(stat.getId(), stat.getDistance(),
                                stat.getDuration(), stat.getSessions());
                    })
                    .observeOn(Schedulers.computation());
    }

    Observable<Long> getDailyStatId(long userId, long date){
        return Observable.defer(() -> {
            Cursor cursor = null;
            try{
                cursor = dailyStatsApi
                        .getStatOnDate(userId, date);
                if (cursor.moveToFirst()) {
                    long id = DailyStatsApi.getId(cursor);
                    return Observable.just(id);
                }

                long id = dailyStatsApi.insertDailyStat(userId,
                        TimeUtil.getToday(date), 0, 0, 0);

                return Observable.just(id);
            }finally {
                if (cursor != null){
                    cursor.close();
                }
            }
        }).subscribeOn(scheduler);
    }

    Observable<? extends ActivitySession> getSessions(long userId, long date){
        long from = getToday(date);
        long to = getTomorrow(from);
        return  sessionsProvider.getSessionIds(userId, from, to)
                .subscribeOn(scheduler)
                .flatMap(sessionsProvider::getSessionData)
                .onBackpressureBuffer();
    }

    public static DailyStat newDailyStat(long id, long date){
        return new DailyStatsItem(id, date);
    }

    public static DailyStat newDailyStat(Cursor cursor){
        long id = DailyStatsApi.getId(cursor);
        long date = DailyStatsApi.getDate(cursor);
        double distance = DailyStatsApi.getDistance(cursor);
        double duration = DailyStatsApi.getDuration(cursor);
        int sessions = DailyStatsApi.getNumberOfSessions(cursor);
        return new DailyStatsItem(id, date, sessions,
                distance, duration);
    }
}
