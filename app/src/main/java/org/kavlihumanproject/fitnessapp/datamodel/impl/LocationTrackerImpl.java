package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.location.Location;
import android.os.Build;
import android.os.Handler;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

import org.kavlihumanproject.fitnessapp.datamodel.AppException;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;
import rx.android.schedulers.HandlerScheduler;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;

public class LocationTrackerImpl implements LocationTracker, LocationListener {

    private final GoogleApiProvider provider;
    private final ReplaySubject<LocationData> replaySubject;
    private final Handler handler;

    public LocationTrackerImpl(GoogleApiProvider provider, Handler handler) {
        this.provider = provider;
        this.handler = handler;
        this.replaySubject = ReplaySubject.create();
    }

    @Override
    public Observable<LocationData> startTracking() {
        if (provider.isReleased()){
            throw new IllegalStateException("Tried to get location updates but the GoogleApiClient was released");
        }

        final HandlerScheduler scheduler
                = HandlerScheduler.from(handler);

        return provider.connect()
                .retryWhen(new RetryWithDelay(10, 30)) /* if the app doesn't have ACCESS_FINE_LOCATION, give the user 5 minutes */
                .filter(GoogleApiClient::isConnected) /* (retrying every 30 seconds) to enable the permission before giving up */
                .flatMap(client -> {
                    Status status = ensureGpsSettingsEnabled(client);
                    if (!status.isSuccess()){
                        return Observable
                                .error(new GoogleApiException(status));
                    }

                    if(requestLocationUpdates(client)) {
                        return replaySubject
                                    .doOnCompleted(provider::release);
                    }

                    return Observable
                            .error(new AppException(1)); /* User revoked ACCESS_FINE_LOCATION somewhere along the way */
                }).subscribeOn(Schedulers.computation())
                  .observeOn(scheduler);
    }

    boolean requestLocationUpdates(GoogleApiClient client) {
        if (checkSelfPermission(client.getContext(), ACCESS_FINE_LOCATION) == PERMISSION_GRANTED){
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(client, buildRequest(), this, handler.getLooper());
            return true;
        }
        return false;
    }

    Status ensureGpsSettingsEnabled(GoogleApiClient client){
        LocationSettingsRequest settingsRequest
                = new LocationSettingsRequest.Builder()
                .addLocationRequest(buildRequest())
                .setAlwaysShow(true)
                .build();
        return LocationServices.SettingsApi
                .checkLocationSettings(client, settingsRequest)
                .await()
                .getStatus();
    }

    private LocationRequest buildRequest(){
        return new LocationRequest()
                .setInterval(5000)
                .setFastestInterval(2500)
                .setSmallestDisplacement(2.5f)
                .setPriority(PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void stopTracking() {
        if (provider.apiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(provider.apiClient, this);
        }
        replaySubject.onCompleted();
    }

    private void stopHandler(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            handler.getLooper().quitSafely();
        }else {
            handler.getLooper().quit();
        }
    }

    @Override
    public void release() {
        if (!replaySubject.hasCompleted()){
            replaySubject.onCompleted();
        }

        if (!provider.isReleased()) {
            provider.release();
        }

        stopHandler();
    }

    @Override
    public void onLocationChanged(Location location) {
        replaySubject.onNext(newData(location));
    }

    private LocationData newData(Location l){
        return new SpaceTime(l.getLatitude(), l.getLongitude(), l.getTime());
    }
}
