package org.kavlihumanproject.fitnessapp.datamodel.impl.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import org.kavlihumanproject.fitnessapp.BootReceiverModule;
import org.kavlihumanproject.fitnessapp.dagger.ApplicationScope;
import org.kavlihumanproject.fitnessapp.tasks.TrackerServiceComponent;
import org.kavlihumanproject.fitnessapp.ui.DailyStatsComponent;
import org.kavlihumanproject.fitnessapp.ui.LoginComponent;
import org.kavlihumanproject.fitnessapp.ui.LeaderBoardComponent;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module(
        subcomponents = {
                LoginComponent.class,
                DailyStatsComponent.class,
                LeaderBoardComponent.class,
                TrackerServiceComponent.class,
                BootReceiverModule.BootReceiverComponent.class
        }
)
public class DatabaseModule {
    private final Context context;

    public DatabaseModule(@NonNull Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides @ApplicationScope
    DbHelper provideDbHelper(){
        return DbHelper.getHelper(context);
    }

    @Provides @ApplicationScope
    SQLiteDatabase provideDatabase(DbHelper helper){
        return helper.getWritableDatabase();
    }

    @Provides @ApplicationScope
    Scheduler provideScheduler(DbHelper helper){
        return helper.getScheduler();
    }

    @Provides @ApplicationScope
    UserTableApi providUserApi(DbHelper helper){
        return helper.getUserTableApi();
    }

    @Provides @ApplicationScope
    SessionDataApi getSessionApi(DbHelper helper){
        return helper.getSessionDataApi();
    }

    @Provides @ApplicationScope
    DailyStatsApi getStatsApi(DbHelper helper){
        return helper.getDailyStatsApi();
    }

}
