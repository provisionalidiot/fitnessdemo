package org.kavlihumanproject.fitnessapp.datamodel;


import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;

public interface LocationTracker {
    Observable<LocationData> startTracking();
    void stopTracking();
    void release();
}
