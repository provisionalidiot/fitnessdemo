package org.kavlihumanproject.fitnessapp.datamodel.impl;

import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.model.LeaderboardModel;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class LeaderboardPresenter {
    private final LeaderboardModel model;
    private final LocalUserProvider provider;

    private Subscription subscription;

    public LeaderboardPresenter(LeaderboardModel model, LocalUserProvider provider) {
        this.model = model;
        this.provider = provider;
    }

    public void getLeaderboard(){
        subscription = provider.calculateLeaderBoard()
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setLeaderboard, this::handleError);
    }


    public void release(){
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }

    void setLeaderboard(List<LocalUser> users){
        model.leaderboard.clear();
        model.leaderboard.addAll(users);
    }

    void handleError(Throwable t){
        t.printStackTrace();
    }
}
