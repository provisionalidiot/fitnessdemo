package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationServices;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class GoogleApiProvider implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    final GoogleApiClient apiClient;
    final private PublishSubject<GoogleApiClient> publish;

    public GoogleApiProvider(Context context) {
        this.apiClient = buildAPIClient(context);
        this.publish = PublishSubject.create();
    }

    private GoogleApiClient buildAPIClient(Context context){
        return new GoogleApiClient.Builder(context)
                .addApi(ActivityRecognition.API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    Observable<GoogleApiClient> connect(){
        if (publish.hasCompleted()){
            throw new IllegalStateException("The provder has already been released");
        }

        return Observable.defer(() -> {
            if (apiClient.isConnected()) {
                return Observable.just(apiClient);
            }
            return publish.serialize()
                    .doOnSubscribe(apiClient::connect);
        });
    }

    boolean isReleased(){
        return publish.hasCompleted();
    }

    public void release(){
        Schedulers.io().createWorker().schedule(() -> {
            if (!publish.hasCompleted()) {
                publish.onCompleted();
            }
            apiClient.disconnect();
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Schedulers.io().createWorker().schedule(() -> {
            if (!publish.hasCompleted()) {
                publish.onNext(apiClient);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Schedulers.io().createWorker().schedule(() -> {
            if (!publish.hasCompleted()) {
                publish.onNext(apiClient);
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Schedulers.io().createWorker().schedule(() -> {
            publish.onError(new GoogleApiException(connectionResult));
        });
    }

}
