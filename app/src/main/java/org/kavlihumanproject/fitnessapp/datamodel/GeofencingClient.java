package org.kavlihumanproject.fitnessapp.datamodel;

import com.google.android.gms.location.Geofence;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;

public interface GeofencingClient {

    String GEOFENCE_TRIGGER_ACTION = "org.kavlihumanproject.fitnessapp.datamodel.ACTION_TRIGGER";

    Observable<LocationData> getCurrentLocation();
    Observable<Boolean> registerGeofence(GeoFence fence);
    Observable<Boolean> unregisterGeofence();
    void release();

    interface GeoFence {
        String getKey();
        double getLat();
        double getLng();
    }

    static GeoFence convertToGeofence(LocationData data){
        String key = Geofence.class.getCanonicalName() + ".OFFICE";
        return  new GeoFence() {
            @Override
            public String getKey() {
                return key;
            }

            @Override
            public double getLat() {
                return data.getLat();
            }

            @Override
            public double getLng() {
                return data.getLng();
            }
        };
    }
}
