package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.content.Context;

import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.tasks.ActivityTrackerService;

import rx.Observable;

public class LiveUpdatesProvider {
    private final Context context;

    public LiveUpdatesProvider(Context context) {
        this.context = context;
    }

    Observable<DailyStat> getLiveUpdates(){
        return ActivityTrackerService
                .listenForUpdates(context);
    }
}
