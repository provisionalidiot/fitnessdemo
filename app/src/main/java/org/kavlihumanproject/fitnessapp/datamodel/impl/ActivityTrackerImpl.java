package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.PendingIntent;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;

import rx.Observable;
import rx.schedulers.Schedulers;

import static com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi;
import static org.kavlihumanproject.fitnessapp.tasks.ActivityTrackerService.buildIntent;

public class ActivityTrackerImpl implements FitActivityTracker {

    private static final long PERIOD = 5*1000; // five minutes is good ?
    private final GoogleApiProvider provider;

    public ActivityTrackerImpl(GoogleApiProvider googleApiProvider){
        this.provider = googleApiProvider;
    }

    @Override
    public Observable<Long> listenForActivities(long userId) {
        return provider.connect()
                .filter(GoogleApiClient::isConnected)
                .flatMap(client -> {
                    Status status = listen(userId, client);
                    return status.isSuccess()
                            ? Observable.just(userId)
                            : Observable.error(new GoogleApiException(status));
                });
    }

    Status listen(long userId, GoogleApiClient client){
        PendingIntent pi = buildIntent(userId, client.getContext());
        PendingResult<Status> pendingResult = ActivityRecognitionApi
                .requestActivityUpdates(client, PERIOD, pi);
        return pendingResult.await();
    }

    @Override
    public Observable<Boolean> stopListening() {
        if (!provider.isReleased()){
            return provider.connect()
                    .filter(GoogleApiClient::isConnected)
                    .map(this::stopUpdates)
                    .subscribeOn(Schedulers.io());
        }
        return Observable.just(false);
    }

    @Override
    public void release() {
        if (!provider.isReleased()){
            provider.release();
        }
    }

    boolean stopUpdates(GoogleApiClient client){
        PendingResult<Status> status = ActivityRecognitionApi
                .removeActivityUpdates(client,
                        buildIntent(0, client.getContext()));
        status.await();
        return !status.isCanceled();
    }
}
