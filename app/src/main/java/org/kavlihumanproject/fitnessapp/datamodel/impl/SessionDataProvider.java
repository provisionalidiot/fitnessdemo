package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.database.Cursor;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.datamodel.impl.SessionData.Builder;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.SessionDataApi;

import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class SessionDataProvider {

    private final SessionDataApi api;
    private final Scheduler scheduler;

    @Inject
    public SessionDataProvider(SessionDataApi api, Scheduler scheduler) {
        this.api = api;
        this.scheduler = scheduler;
    }

    public Observable<Long> getAllSessionIds(long userId){
        return Observable.defer(() -> {
            Cursor sessions = api.getSessionsIds(userId);
            return Observable.create(
                    new CursorOnSubscribe<>(sessions, SessionDataApi::getSessionId))
                    .onBackpressureBuffer();
        }).subscribeOn(scheduler);
    }

    public Observable<Long> getSessionIds(long userId, long from, long to){
        return Observable.defer(() -> {
            Cursor sessions = api.getSessionsBetween(userId, from, to);
            return Observable.create(
                    new CursorOnSubscribe<>(sessions, SessionDataApi::getSessionId))
                    .onBackpressureBuffer();
        });
    }

    public Observable<ActivitySession> getSessionData(long sessionId) {
       return Observable.defer(() -> queryDataStream(sessionId))
               .subscribeOn(scheduler)
               .cache()
               .observeOn(Schedulers.computation())
               .compose(Builder::create)
               .map(builder -> builder.setSessionId(sessionId))
               .map(Builder::build)
               .doOnNext(session -> {
                    scheduler.createWorker().schedule(() -> {
                       api.updateSession(sessionId,
                               session.getDistance(), session.getSessionEnd());
                    });
               });
    }

    Observable<LocationData> queryDataStream(long sessionId){
        Cursor data = api.getDataFromSession(sessionId);
        return Observable
                .create(new CursorOnSubscribe<>(data, this::newData))
                .defaultIfEmpty(new SpaceTime(0, 0, 0))
                .onBackpressureBuffer();
    }

    LocationData newData(Cursor cursor){
        double lat = SessionDataApi.getLat(cursor);
        double lng = SessionDataApi.getLng(cursor);
        long timestamp = SessionDataApi.getTimestamp(cursor);
        return new SpaceTime(lat, lng, timestamp);
    }
}
