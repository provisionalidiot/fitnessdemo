package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;

import dagger.Module;
import dagger.Provides;

@Module
public class GooglePlayServicesModule {

    private static final String HANDLER_NAME
            = "org.kavlihumnaproject.fitnessapp.LocationTrackerHandlerThread";

    private final Context context;

    public GooglePlayServicesModule(Context context) {
        this.context = context;
    }

    @Provides
    public Handler provideHandler(){
        HandlerThread thread = new HandlerThread(HANDLER_NAME);
        thread.start();
        return new Handler(thread.getLooper());
    }

    @Provides
    public GoogleApiProvider providerGoogleApi(){
        return new GoogleApiProvider(context);
    }

    @Provides
    public FitActivityTracker provideFitActivityTracker(GoogleApiProvider googleApiProvider){
        return new ActivityTrackerImpl(googleApiProvider);
    }

    @Provides
    public LocationTracker provideLocationTracker(GoogleApiProvider provider, Handler handler){
        return new LocationTrackerImpl(provider, handler);
    }

    @Provides
    GeofencingClient provideGeofencingClient(GoogleApiProvider provider, LocationTracker tracker) {
        return new GeofencingClientImpl(provider, tracker);
    }

}
