package org.kavlihumanproject.fitnessapp.datamodel;

import android.support.annotation.NonNull;

import rx.Observable;

public interface PermissionHelper {
    boolean hasPermission();
    <T> Observable<T> requestPermission(T t);
    void handleResult(boolean granted);
    void cancelRequest();
    boolean isReleased();
    void handleRequestResults(int requestCode, @NonNull String[] permissions,
                                 @NonNull int[] grantResults);
}
