package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.Activity;
import android.content.Intent;

import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.MainActivity;
import org.kavlihumanproject.fitnessapp.ui.LoginScreenActivity;
import org.kavlihumanproject.fitnessapp.ui.SplashActivity;

public class ActivityNavigator implements ScreenNavigator {

    private static final int RESOLVE_GOOGLE_API_ERROR = 503;

    private Activity activity;
    private boolean isReleased;

    public ActivityNavigator(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void gotoMainScreen(LocalUser activeUser) {
        if (isReleased)
            throw new IllegalStateException("Screen navigator has already been released");

        MainActivity.launch(activity, activeUser);
        activity.finish();
    }

    @Override
    public void gotoLoginPage() {
        if (isReleased)
            throw new IllegalStateException("Screen navigator has already been released");
        Intent intent = new Intent(activity, LoginScreenActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void relaunchSplash() {
        if (isReleased)
            throw new IllegalStateException("Screen navigator has already been released");

        Intent intent =  new Intent(activity, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    @Override
    public void release() {
        isReleased = true;
        activity = null;
    }

    @Override
    public boolean isReleased() {
        return isReleased;
    }

    public void handle(GoogleApiException e){
        if (isReleased){
            throw new IllegalStateException("Screen navigator was already released");
        }

        e.resolve(activity, RESOLVE_GOOGLE_API_ERROR);
    }

}
