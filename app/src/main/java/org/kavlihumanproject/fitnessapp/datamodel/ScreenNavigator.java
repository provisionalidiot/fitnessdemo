package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GoogleApiException;

public interface ScreenNavigator {
    void gotoMainScreen(LocalUser active);
    void gotoLoginPage();
    void relaunchSplash();
    void release();
    boolean isReleased();
    void handle(GoogleApiException e);
}
