package org.kavlihumanproject.fitnessapp.datamodel;

public interface LoginPresenter {
    void imNewButtonClicked();
    void cancelButtonClicked();
    void login();
    void createNewUser();
    void cancel();
}
