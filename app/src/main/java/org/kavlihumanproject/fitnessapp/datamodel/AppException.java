package org.kavlihumanproject.fitnessapp.datamodel;

import android.support.annotation.StringRes;

public class AppException extends RuntimeException {
    private final @StringRes int errorMessage;

    public AppException(int errorMessage) {
        this.errorMessage = errorMessage;
    }

    public @StringRes int getErrorMessage() {
        return errorMessage;
    }
}
