package org.kavlihumanproject.fitnessapp.datamodel.impl;


import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

public class User implements LocalUser {
    private final long id;
    private final String username;
    private final double distance;
    private  double officeLat;
    private  double officeLng;

    public User(long id, String username) {
        this.id = id;
        this.username = username;
        this.distance = 0;
    }

    public User(long userId, String username, double distance) {
        this.id = userId;
        this.username = username;
        this.distance = distance;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public boolean hasOffice() {
        return !(officeLat == 0 && officeLng == 0);
    }

    @Override
    public LocationData getOfficeLocation() {
        return new SpaceTime(officeLat, officeLng, 0);
    }

    public void setOfficeLng(double officeLng) {
        this.officeLng = officeLng;
    }

    public void setOfficeLat(double officeLat) {
        this.officeLat = officeLat;
    }

    @Override
    public LocalUser setDistance(double distance) {
        return new User(id, username, distance);
    }

    @Override
    public String toString() {
        return "userId = " + id + " username " + username + " distance " + distance;
    }
}
