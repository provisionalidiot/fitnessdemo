package org.kavlihumanproject.fitnessapp.datamodel;

public interface NotificationPresenter {
    void showMilestoneNotification(int milestone);
    void showReminderNotificaion();
}
