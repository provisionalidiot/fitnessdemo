package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.database.Cursor;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.SessionDataApi;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

public class SessionData implements ActivitySession {
    private final long sessionId;
    private final long sessionStart;
    private final long sessionEnd;
    private final double distance;

    private SessionData(long sessionId, long sessionStart, long sessionEnd, double distance) {
        this.sessionId = sessionId;
        this.sessionStart = sessionStart;
        this.sessionEnd = sessionEnd;
        this.distance = distance;
    }

    public SessionData(Cursor cursor){
        this.sessionId = SessionDataApi.getSessionId(cursor);
        this.sessionStart = SessionDataApi.getSessionStart(cursor);
        this.sessionEnd = SessionDataApi.getSessionEnd(cursor);
        this.distance = SessionDataApi.getSessionDistance(cursor);
    }

    @Override
    public long getSessionId() {
        return sessionId;
    }

    @Override
    public long getSessionStart() {
        return sessionStart;
    }

    @Override
    public long getSessionEnd() {
        return sessionEnd;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public long getDuration() {
        return sessionEnd - sessionStart;
    }

    @Override
    public String toString() {
        String data = "Session Id = " + sessionId + "\n";
        data += "Distance traveled = " + distance + "\n";
        data += "Session start = " + TimeUtil.print(sessionStart) + "\n";
        data += "Session end = " +  TimeUtil.print(sessionEnd);
        return data;
    }

    static final class Builder {

        private final Observable<LocationData> data;

        private long sessionId;
        private long sessionStart;
        private long sessionEnd;
        private double distance;

        public static Observable<Builder> create(Observable<LocationData> data){;
            Builder builder = new Builder(data);
            List<Observable<Builder>> observables = Arrays.asList(builder.setSessionStart(),
                    builder.setSessionEnd(),
                    builder.setDistance());
            return Observable.zip(observables, args -> builder);
        }

        private Builder(Observable<LocationData> data){
            this.data = data;
        }

        public Builder(){
            data = null;
        }

        public Builder setSessionId(long sessionId){
            this.sessionId = sessionId;
            return this;
        }

        Observable<Builder> setSessionStart(){
            return data.first()
                    .map(LocationData::timestamp)
                    .map(this::setSessionStart);
        }

        Builder setSessionStart(long sessionStart){
            this.sessionStart = sessionStart;
            return this;
        }

        Observable<Builder> setSessionEnd(){
            return data.last()
                    .map(LocationData::timestamp)
                    .map(this::setSessionEnd);
        }

        Builder setSessionEnd(long sessionEnd){
            this.sessionEnd = sessionEnd;
            return this;
        }

        Observable<Builder> setDistance(){
            return data
                    .compose(ActivitySession::delta)
                    .compose(ActivitySession::sum)
                    .map(this::setDistance);
        }

        Builder setDistance(double distance){
            this.distance = distance;
            return this;
        }


        ActivitySession build(){
            /**
             * Todo: Put in validation
             */
            return new SessionData(sessionId, sessionStart, sessionEnd, distance);
        }
    }
}
