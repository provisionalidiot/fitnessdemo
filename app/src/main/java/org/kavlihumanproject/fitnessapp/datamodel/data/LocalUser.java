package org.kavlihumanproject.fitnessapp.datamodel.data;

public interface LocalUser {
    long getId();
    String getUsername();
    double getDistance();
    boolean hasOffice();
    LocationData getOfficeLocation();
    LocalUser setDistance(double distance);
}
