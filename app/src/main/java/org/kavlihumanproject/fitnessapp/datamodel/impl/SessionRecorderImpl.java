package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.database.Cursor;

import org.kavlihumanproject.fitnessapp.datamodel.SessionRecorder;
import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.SessionDataApi;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import rx.Observable;
import rx.Scheduler;

import static org.kavlihumanproject.fitnessapp.datamodel.impl.DailyStatsProvider.newDailyStat;

public class SessionRecorderImpl implements SessionRecorder {

    private final SessionDataApi sessionApi;
    private final Scheduler scheduler;
    private final DailyStatsApi statsApi;

    private DailyStat currentStat;
    private long sessionId;
    private long activeUserId;
    private boolean isRecording;

    public SessionRecorderImpl(SessionDataApi sessionApi, DailyStatsApi statsApi, Scheduler scheduler) {
        this.sessionApi = sessionApi;
        this.statsApi = statsApi;
        this.scheduler = scheduler;
    }

    @Override
    public boolean isRecording(){
        return isRecording;
    }

    @Override
    public Observable<Long> startRecording(long userId){
        if (isRecording){
            return Observable.error(new IllegalStateException("Session Recorder is already recording"));
        }

        activeUserId = userId;
        isRecording = true;
        return Observable.defer(() -> {
            getStartingDistance(userId);
            sessionId = sessionApi.startSession(userId);
            if (sessionId < 0) {
                return Observable.error(new RuntimeException("Could not create session"));
            }

            return Observable.just(sessionId);
        }).subscribeOn(scheduler);
    }

    void getStartingDistance(long userId){
        Cursor cursor = null;
        try {
            long today = TimeUtil.getToday();
            cursor = statsApi
                    .getStatOnDate(userId, today);
            if (cursor.moveToFirst()){
                currentStat = newDailyStat(cursor);
            }else {
                long dailyStatId = statsApi
                        .insertDailyStat(userId, today, 0, 0, 0);
                currentStat = newDailyStat(dailyStatId, today);
            }
        }finally {
            if (cursor != null){
                cursor.close();
            }
        }
    }

    @Override
    public Observable<Boolean> liveUpdateStats(ActivitySession session){
       return Observable.fromCallable(() -> statsApi.updateDailyStat(currentStat.getId(), currentStat.getDistance() + session.getDistance(),
              currentStat.getDuration() + (double) session.getDuration() / (60 * 60 * 100),
              currentStat.getSessions() + 1))
        .subscribeOn(scheduler);
    }

    @Override
    public Observable<LocationData> recordData(LocationData data){
        if (!isRecording) {
            throw new IllegalStateException("Recorder is not running");
        }
        return Observable.fromCallable(() -> {
            sessionApi.insertLocationData(activeUserId, sessionId,
                    data.getLat(), data.getLng(), data.timestamp());
            return data;
        }).subscribeOn(scheduler);
    }

    @Override
    public void stopRecording(){
        if (!isRecording) {
            throw new IllegalStateException("Trying to stop a not running session");
        }
        isRecording = false;
    }

    @Override
    public DailyStat getStartingStat() {
        return currentStat;
    }
}
