package org.kavlihumanproject.fitnessapp.datamodel.data;

import rx.Observable;

public interface ActivitySession {
    long getSessionId();
    long getSessionStart();
    long getSessionEnd();
    double getDistance();
    long getDuration();


    static Observable<Double> delta(Observable<LocationData> obs){
        return obs.zipWith(obs.skip(1), LocationData::distance);
    }

    static Observable<Double> sum(Observable<Double> obs){
        return obs.reduce(0.0, (sum, next) -> sum + next);
    }
}
