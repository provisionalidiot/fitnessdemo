package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;

import rx.Observable;

public interface LocalUserProvider {
    Observable<LocalUser> verifyLogin(String username, String password);
    Observable<LocalUser> createNewUser(String username, String password);
    Observable<LocalUser> getLeaderboard();
    Observable<LocalUser> getAllUsers();
    Observable<LocalUser> calculateLeaderBoard();
}
