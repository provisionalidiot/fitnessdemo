package org.kavlihumanproject.fitnessapp.datamodel.impl.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import javax.inject.Inject;

import static org.kavlihumanproject.fitnessapp.datamodel.impl.database.DbHelper.*;
import static org.kavlihumanproject.fitnessapp.util.TimeUtil.assertDateTimestamp;

public class DailyStatsApi {

    public static final String TABLE_DAILY_NAME = "daily_statistics";
    public static final String KEY_DAILY_PRIMARY = "id";
    public static final String KEY_DAILY_USER_ID = "user_id";
    public static final String COL_DAILY_DATE = "date";
    public static final String COL_DAILY_DISTANCE = "distance";
    public static final String COL_DAILY_DURATION = "duration";
    public static final String COL_DAILY_SESSIONS = "sessions";

    private static final String CREATE_DAILY_TABLE = "CREATE TABLE " + TABLE_DAILY_NAME
            + "(" + KEY_DAILY_PRIMARY +" INTEGER PRIMARY KEY AUTOINCREMENT"
            + ", " + COL_DAILY_DATE + " INTEGER NOT NULL"
            + ", " + COL_DAILY_DURATION + " REAL DEFAULT 0.0"
            + ", " + COL_DAILY_DISTANCE + " REAL DEFAULT 0.0"
            + ", " + COL_DAILY_SESSIONS + " INTEGER DEFAULT 0"
            + ", " + KEY_DAILY_USER_ID + " INTEGER NOT NULL"
            + ", UNIQUE (" + KEY_DAILY_USER_ID + ", " + COL_DAILY_DATE + ")"
            + ", FOREIGN KEY(" + KEY_DAILY_USER_ID + ") " +
            "REFERENCES "+UserTableApi.TABLE_USER_NAME +"("+UserTableApi.KEY_PRIMARY+"));";


    private static final String DAILY_INDEX = "daily_stat_idx";
    private static  final String CREATE_DAILY_INDEX = "CREATE INDEX " + DAILY_INDEX +
            " on " + TABLE_DAILY_NAME + "(" + COL_DAILY_DATE + ", "+ KEY_DAILY_USER_ID + ");";

    private final SQLiteDatabase db;

    @Inject
    DailyStatsApi(SQLiteDatabase db){
        this.db = db;
    }

    void create(){
        assertRunningOnTheDbScheduler();
        db.execSQL(CREATE_DAILY_TABLE);
        db.execSQL(CREATE_DAILY_INDEX);
    }

    void drop(){
        assertRunningOnTheDbScheduler();
        String dropIndex = "DROP INDEX IF EXISTS " + DAILY_INDEX + ";";
        String drop = "DROP TABLE IF EXISTS " + TABLE_DAILY_NAME + ";";
        db.execSQL(dropIndex);
        db.execSQL(drop);
    }

    public Cursor getStatOnDate(long userId, long date){
        assertRunningOnTheDbScheduler();
        assertDateTimestamp(date);

        final String [] columns = {KEY_DAILY_PRIMARY, COL_DAILY_DATE, COL_DAILY_SESSIONS,
                COL_DAILY_DISTANCE, COL_DAILY_DURATION};

        final String where = KEY_DAILY_USER_ID + " = " + userId +
                " AND " + COL_DAILY_DATE  + " = " + date;

        return db.query(TABLE_DAILY_NAME, columns, where, null,
                null, null, null, null);
    }

    public Cursor getDailyStatistics(long userId, long latest, int count){
        assertRunningOnTheDbScheduler();
        final String [] columns = {KEY_DAILY_PRIMARY, COL_DAILY_DATE, COL_DAILY_SESSIONS,
                COL_DAILY_DISTANCE, COL_DAILY_DURATION};

        final String where = KEY_DAILY_USER_ID + " = " + userId +
                " AND " + COL_DAILY_DATE  + " < " + latest;

        final String orderBy = COL_DAILY_DATE + " DESC";

        final String limit = "" + count;

        return db.query(TABLE_DAILY_NAME, columns, where, null,
                null, null, orderBy, limit);
    }

    public boolean deleteStatsForUser(long userId){
        final String where = KEY_DAILY_PRIMARY  + " = " + userId;
        int delete = db.delete(TABLE_DAILY_NAME, where, null);
        return delete != -1;
    }

    public long insertDailyStat(long userId, long date, double distance,
                                   double duration, int sessions){

        assertRunningOnTheDbScheduler();
        assertDateTimestamp(date);

        ContentValues values = new ContentValues();
        values.put(KEY_DAILY_USER_ID, userId);
        values.put(COL_DAILY_DATE, TimeUtil.getToday(date));
        values.put(COL_DAILY_DURATION, duration);
        values.put(COL_DAILY_DISTANCE, distance);
        values.put(COL_DAILY_SESSIONS, sessions);
        return db.insert(TABLE_DAILY_NAME, null, values);
    }

    public boolean updateDailyStat(long id, double distance,
                                double duration, int sessions){

        assertRunningOnTheDbScheduler();

        ContentValues values = new ContentValues();
        values.put(COL_DAILY_DURATION, duration);
        values.put(COL_DAILY_DISTANCE, distance);
        values.put(COL_DAILY_SESSIONS, sessions);

        final String where = KEY_DAILY_PRIMARY + " = " + id;
        int count = db.update(TABLE_DAILY_NAME, values, where, null);

        return count == 1;
    }

    public static long getId(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(KEY_DAILY_PRIMARY));
    }

    public static long getDate(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(COL_DAILY_DATE));
    }

    public static double getDistance(Cursor cursor){
        return cursor.getDouble(cursor.getColumnIndex(COL_DAILY_DISTANCE));
    }

    public static double getDuration(Cursor cursor){
        return cursor.getDouble(cursor.getColumnIndex(COL_DAILY_DURATION));
    }

    public static int getNumberOfSessions(Cursor cursor){
        return cursor.getInt(cursor.getColumnIndex(COL_DAILY_SESSIONS));
    }

    public static long getUserId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(KEY_DAILY_USER_ID));
    }
}
