package org.kavlihumanproject.fitnessapp.datamodel.impl.database;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

public class SessionDataApi {

    public static final String TABLE_SESSION_NAME = "session";
    public static final String KEY_SESSION_PRIMARY = "id";
    public static final String KEY_SESSION_USER_ID = "user_id";
    public static final String COL_SESSION_DISTANCE = "distance";
    public static final String COL_SESSION_START = "start_timestamp";
    public static final String COL_SESSION_END = "end_timestamp";

    private static final String CREATE_SESSION_TABLE = "CREATE TABLE " + TABLE_SESSION_NAME
            + "(" + KEY_SESSION_PRIMARY +" INTEGER PRIMARY KEY AUTOINCREMENT"
            + ", " + COL_SESSION_START + " INTEGER NOT NULL"
            + ", " + COL_SESSION_END + " INTEGER DEFAULT 0"
            + ", " + COL_SESSION_DISTANCE + " REAL DEFAULT 0.0"
            + ", " + KEY_SESSION_USER_ID + " INTEGER NOT NULL"
            + ", FOREIGN KEY(" + KEY_SESSION_USER_ID + ") " +
                    "REFERENCES "+UserTableApi.TABLE_USER_NAME +"("+UserTableApi.KEY_PRIMARY+"));";


    private static final String SESSION_INDEX = "session_idx";
    private static  final String CREATE_SESSION_INDEX = "CREATE INDEX " + SESSION_INDEX +
            " on " + TABLE_SESSION_NAME + "(" + COL_SESSION_START + ", "+ KEY_SESSION_USER_ID+");";


    public static final String TABLE_DATA_NAME = "location_data";
    public static final String KEY_DATA_PRIMARY = "id";
    public static final String KEY_DATA_USER_ID = "user_id";
    public static final String KEY_DATA_SESSION_ID = "session_id";
    public static final String COL_DATA_LATITUDE = "latitude";
    public static final String COL_DATA_LONGITUDE = "longitude";
    public static final String COL_DATA_TIMESTAMP = "timestamp";


    private static final String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_DATA_NAME
            + "(" + KEY_DATA_PRIMARY +" INTEGER PRIMARY KEY AUTOINCREMENT"
            + ", " + COL_DATA_LATITUDE +" REAL NOT NULL"
            + ", " + COL_DATA_LONGITUDE + " REAL NOT NULL"
            + ", " + COL_DATA_TIMESTAMP + " INTEGER NOT NULL"
            + ", " + KEY_DATA_USER_ID + " INTEGER NOT NULL"
            + ", " + KEY_DATA_SESSION_ID + " INTEGER NOT NULL"
            + ", FOREIGN KEY(" + KEY_DATA_USER_ID + ") " +
                    "REFERENCES "+ UserTableApi.TABLE_USER_NAME + "("+UserTableApi.KEY_PRIMARY+")"
            + ", FOREIGN KEY(" + KEY_DATA_SESSION_ID + ") " +
                    "REFERENCES " + TABLE_SESSION_NAME + "("+KEY_SESSION_PRIMARY+"));";

    private static final String DATA_INDEX = "location_data_idx";
    private static  final String CREATE_DATA_INDEX = "CREATE INDEX " + DATA_INDEX +
            " on " + TABLE_DATA_NAME + "(" + COL_DATA_TIMESTAMP + ", "+ KEY_DATA_USER_ID+");";


    private final SQLiteDatabase db;

    @Inject
    SessionDataApi(SQLiteDatabase db) {
        this.db = db;
    }

    void create(){
        db.execSQL(CREATE_SESSION_TABLE);
        db.execSQL(CREATE_LOCATION_TABLE);
        db.execSQL(CREATE_SESSION_INDEX);
        db.execSQL(CREATE_DATA_INDEX);
    }

    public long startSession(long userId){
        DbHelper.assertRunningOnTheDbScheduler();
        ContentValues values = new ContentValues();
        values.put(KEY_SESSION_USER_ID, userId);
        values.put(COL_SESSION_START, System.currentTimeMillis());
        return db.insertOrThrow(TABLE_SESSION_NAME, null, values);
    }

    public boolean updateSession(long sessionId, double distance, long end){
        DbHelper.assertRunningOnTheDbScheduler();

        ContentValues values = new ContentValues();
        values.put(COL_SESSION_DISTANCE, distance);
        values.put(COL_SESSION_END, end);

        String where = KEY_SESSION_PRIMARY + " = " + sessionId;

        int result = db.update(TABLE_SESSION_NAME, values, where, null);

        return result > 0;
    }

    public Cursor getSessionsBetween(long userId, long begin, long end){
        if (begin > end){
            throw new AssertionError("begin timestamp must be less than or equal to the end timestamp");
        }

        DbHelper.assertRunningOnTheDbScheduler();


        final String [] columns = {KEY_SESSION_PRIMARY, COL_SESSION_START,
                COL_SESSION_END, COL_SESSION_DISTANCE};

        final String where = KEY_SESSION_USER_ID + " = "+userId +
                " AND " + COL_SESSION_START + " >= " + begin +
                " AND " + COL_SESSION_START + " < " + end;

        final String orderBy = COL_SESSION_START + " ASC";

        return db.query(TABLE_SESSION_NAME, columns, where,
                null, null, null, orderBy, null);
    }

    public Cursor getSessionsIds(long userId) {
        DbHelper.assertRunningOnTheDbScheduler();

        final String [] columns = {KEY_SESSION_PRIMARY};

        final String where = KEY_SESSION_USER_ID + " = "+userId;
        final String orderBy = COL_SESSION_START + " ASC";
        return db.query(TABLE_SESSION_NAME, columns, where,
                null, null, null, orderBy, null);
    }

    public Cursor getDataFromSession(long sessionId){
        DbHelper.assertRunningOnTheDbScheduler();

        final String [] columns = {COL_DATA_LATITUDE, COL_DATA_LONGITUDE, COL_DATA_TIMESTAMP};
        final String where = KEY_DATA_SESSION_ID + " = "+sessionId;

        return db.query(TABLE_DATA_NAME,
                columns, where,
                null, null, null, null);
    }

    public long insertLocationData(long userId, long sessionId,
                                   double lat, double lng, long timestamp){
        DbHelper.assertRunningOnTheDbScheduler();
        ContentValues values = new ContentValues();
        values.put(KEY_DATA_USER_ID, userId);
        values.put(KEY_DATA_SESSION_ID, sessionId);
        values.put(COL_DATA_LATITUDE, lat);
        values.put(COL_DATA_LONGITUDE, lng);
        values.put(COL_DATA_TIMESTAMP, timestamp);
        return db.insert(TABLE_DATA_NAME, null, values);
    }

    public Cursor getUserLocationDataBetween(long userId, long begin, long end){
        if (begin > end){
            throw new AssertionError("begin timestamp must be less than or equal to the end timestamp");
        }

        DbHelper.assertRunningOnTheDbScheduler();

        final String [] columns = {KEY_DATA_SESSION_ID, COL_DATA_LATITUDE,
                COL_DATA_LONGITUDE, COL_DATA_TIMESTAMP};

        final String where = KEY_DATA_USER_ID + " = "+userId +
                " AND " + COL_DATA_TIMESTAMP + " >= " + begin +
                " AND " + COL_SESSION_START + " <= " + end;

        final String orderBy = COL_SESSION_START + " ASC";

        return db.query(TABLE_DATA_NAME, columns, where,
                null, null, null, orderBy, null);
    }

    public static long getSessionId(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(KEY_SESSION_PRIMARY));
    }

    public static long getSessionStart(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(COL_SESSION_START));
    }

    public static long getSessionEnd(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(COL_SESSION_START));
    }

    public static double getSessionDistance(Cursor cursor){
        return cursor.getDouble(cursor.getColumnIndex(COL_SESSION_DISTANCE));
    }

    public static double getLat(Cursor cursor){
        return cursor.getDouble(cursor.getColumnIndex(COL_DATA_LATITUDE));
    }

    public static double getLng(Cursor cursor){
        return cursor.getDouble(cursor.getColumnIndex(COL_DATA_LONGITUDE));
    }

    public static long getTimestamp(Cursor cursor){
        return cursor.getLong(cursor.getColumnIndex(COL_DATA_TIMESTAMP));
    }
}
