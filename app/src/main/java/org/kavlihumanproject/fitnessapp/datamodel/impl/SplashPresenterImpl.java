package org.kavlihumanproject.fitnessapp.datamodel.impl;

import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.PermissionHelper;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.SplashPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.model.SplashScreenModel;
import org.kavlihumanproject.fitnessapp.util.Optional;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashPresenterImpl extends Subscriber<Optional<LocalUser>> implements SplashPresenter {

    private final ScreenNavigator screenNavigator;
    private final LocalUserCache cache;
    private final PermissionHelper helper;
    private final FitActivityTracker tracker;
    private final SplashScreenModel model;

    private Subscription subscription;

    public SplashPresenterImpl(ScreenNavigator screenNavigator,
                               LocalUserCache cache, PermissionHelper helper,
                               FitActivityTracker tracker, SplashScreenModel model) {
        this.screenNavigator = screenNavigator;
        this.cache = cache;
        this.helper = helper;
        this.tracker = tracker;
        this.model = model;
    }

    @Override
    public void checkForActiveUser() {
        model.showLoading(true);
        subscription = cache.getActiveUser()
                .flatMap(user -> {
                  if (user.isPresent()){
                      return helper.requestPermission(user.get().getId())
                              .flatMap(tracker::listenForActivities)
                              .map(id -> user);
                  }
                    return Observable.just(user);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);
    }

    @Override
    public void cancel() {
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }

        if (!helper.isReleased()){
            helper.cancelRequest();
        }

        screenNavigator.release();
    }

    @Override
    public void onNext(Optional<LocalUser> activeUser) {
        model.showLoading(false);
        if (activeUser.isPresent()) {
            screenNavigator.gotoMainScreen(activeUser.get());
        }else {
            screenNavigator.gotoLoginPage();
        }
    }

    @Override
    public void onCompleted() {
        model.showLoading(false);
    }

    @Override
    public void onError(Throwable error) {
        error.printStackTrace();
        if (error instanceof GoogleApiException){
            GoogleApiException exception
                    = (GoogleApiException) error;

            model.setErrorCode(exception.errorCode);
        }
        model.showLoading(false);
    }
}
