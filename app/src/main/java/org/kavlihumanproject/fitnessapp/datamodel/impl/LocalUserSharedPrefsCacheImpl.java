package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.util.Optional;
import org.kavlihumanproject.fitnessapp.util.StringsUtil;

import rx.Observable;

public class LocalUserSharedPrefsCacheImpl implements LocalUserCache {

    private static final String KEY_ID = "user_id";
    private static final String KEY_NAME = "user_name";

    private static final long NO_USER = -1;
    private final SharedPreferences prefs;

    public LocalUserSharedPrefsCacheImpl(@NonNull SharedPreferences prefs) {
        this.prefs = prefs;
    }

    @Override
    public Observable<Optional<LocalUser>> getActiveUser() {
        return Observable.fromCallable(this::getActiveUserSync);
    }

    public Optional<LocalUser> getActiveUserSync(){
        long userId = prefs.getLong(KEY_ID, NO_USER);
        String userName = prefs.getString(KEY_NAME, "");
        if (userId == NO_USER || StringsUtil.isEmpty(userName)){
            prefs.edit().clear().commit();
            return Optional.empty();
        }else {
            User user = new User(userId, userName);
            return Optional.of(user);
        }
    }

    @Override
    public Observable<LocalUser> setActiveUser(LocalUser user) {
        return Observable.fromCallable(() -> {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(KEY_ID, user.getId());
            editor.putString(KEY_NAME, user.getUsername());
            editor.commit();
            return user;
        });
    }

    @Override
    public Observable<Boolean> clearCache() {
        return Observable.fromCallable(() -> {
            prefs.edit().clear().commit();
            return true;
        });
    }


}
