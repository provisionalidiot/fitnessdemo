package org.kavlihumanproject.fitnessapp.datamodel.impl;


import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalPosition;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

public class SpaceTime implements LocationData {

    private final double lat;
    private final double lng;
    private final long timestamp;

    private static final GeodeticCalculator GEO_CALC
            = new GeodeticCalculator();

    public SpaceTime(double lat, double lng, long timestamp) {
        this.lat = lat;
        this.lng = lng;
        this.timestamp = timestamp;
    }

    @Override
    public double getLat() {
        return lat;
    }

    @Override
    public double getLng() {
        return lng;
    }

    @Override
    public long timestamp() {
        return timestamp;
    }

    @Override
    public double distance(LocationData data) {
        GlobalPosition pointA = new GlobalPosition(lat, lng, 0.0);
        GlobalPosition pointB = new GlobalPosition(data.getLat(), data.getLng(), 0.0);
        return GEO_CALC.calculateGeodeticCurve(Ellipsoid.WGS84, pointB, pointA)  /* meters */
                .getEllipsoidalDistance();
    }

    @Override
    public String toString() {
        return "Lat: " + lat +" Lng: "+lng;
    }

}
