package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;

import rx.Observable;

public interface TrackingSession {
    long getSessionId();
    long getUserId();
    boolean isActive();
    Observable<Double> startSession();
    void endSession();
    boolean didReachMilestone();
    int getCurrentMilestone();
    ActivitySession getSessionData();
    DailyStat getUpdatedStats();
}
