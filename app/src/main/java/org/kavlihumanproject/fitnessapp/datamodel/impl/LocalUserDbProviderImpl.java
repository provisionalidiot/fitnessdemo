package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.datamodel.AppException;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.util.HashingUtil;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

public class LocalUserDbProviderImpl implements LocalUserProvider {

    private final UserTableApi userApi;
    private final DailyStatsApi statsApi;
    private final Scheduler scheduler;

    public LocalUserDbProviderImpl(UserTableApi userApi, DailyStatsApi statsApi, Scheduler scheduler){
        this.userApi = userApi;
        this.statsApi =  statsApi;
        this.scheduler = scheduler;
    }

    @Override
    public Observable<LocalUser> verifyLogin(String username, String password) {
        return Observable.fromCallable(() -> verifyCredentials(username, password))
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread());
    }

    LocalUser verifyCredentials(String username, String password){
        Cursor cursor = null;
        try {

            cursor = userApi.getUserByName(username);
            if (!cursor.moveToFirst())
                throw new AppException(R.string.error_username_password);

            if (!checkHash(password, cursor))
                throw new AppException(R.string.error_username_password);

            long userId = UserTableApi.getId(cursor);
            double officeLat = UserTableApi.getOfficeLat(cursor);
            double officeLng = UserTableApi.getOfficeLng(cursor);
            User user = new User(userId, username);
            user.setOfficeLat(officeLat);
            user.setOfficeLng(officeLng);
            return user;
        }catch (SQLiteException e){
            e.printStackTrace();
            throw new AppException(R.string.error_verify_user_unknown_database);
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    boolean checkHash(String password, Cursor cursor){
        String hash = UserTableApi.getHash(cursor);
        byte[] salt = UserTableApi.getSalt(cursor);
        return hash.equals(HashingUtil.sha1Hash(password, salt));
    }

    @Override
    public Observable<LocalUser> createNewUser(String username, String password) {
        return Observable.fromCallable(() -> newUser(username, password))
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread());
    }

    LocalUser newUser(String username, String password) {
        try {
            byte[] salt = HashingUtil.generateSalt();
            String hash = HashingUtil.sha1Hash(password, salt);
            long id = userApi.createUser(username, hash, salt);
            if (id < 0) { // failed to create user
                throw new AppException(R.string.error_existing_user);
            }
            return new User(id, username);
        }catch (SQLiteConstraintException e){
            e.printStackTrace();
            throw new AppException(R.string.error_existing_user);
        }catch (SQLiteException e){
            e.printStackTrace();
            throw new AppException(R.string.error_create_user_unknown_database);
        }
    }

    @Override
    public Observable<LocalUser> getAllUsers(){
        return Observable.defer(() -> {
            Cursor users = userApi.getAllUsers();
            return Observable
                    .create(new CursorOnSubscribe<>(users, this::newUser))
                    .onBackpressureBuffer();
        }).subscribeOn(scheduler);
    }

    @Override
    public Observable<LocalUser> calculateLeaderBoard(){
        return getAllUsers()
                .flatMap(user -> {
                    Cursor userStats = statsApi.getDailyStatistics(
                            user.getId(), System.currentTimeMillis(), 500 * 365); // Accurate up to last five hundreds years
                    return Observable.create(new CursorOnSubscribe<>(userStats,
                            DailyStatsProvider::newDailyStat))
                            .onBackpressureBuffer()
                            .reduce(0.0, (total, stat) -> total + stat.getDistance())
                            .map(user::setDistance);
                }).subscribeOn(scheduler);
    }

    @Override
    public Observable<LocalUser> getLeaderboard(){
        return Observable.defer(() -> {
            Cursor leaderBoard = userApi.getLeaderBoardAllTime();
            return Observable
                    .create(new CursorOnSubscribe<>(leaderBoard, this::newUser))
                    .onBackpressureBuffer();
        }).subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread());
    }

    LocalUser newUser(Cursor cursor){
        long userId = UserTableApi.getId(cursor);
        String username = UserTableApi.getUsername(cursor);
        double distance = UserTableApi.getDistance(cursor);
        return new User(userId, username, distance);
    }

}
