package org.kavlihumanproject.fitnessapp.datamodel.impl;

import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.SessionRecorder;
import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;
import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;
import rx.functions.Func1;

public class TrackingSessionImpl implements TrackingSession {
    private static final int MILESTONE_MULT = 1000; // Meters

    private final LocationTracker tracker;
    private final SessionRecorder recorder;
    private final long userId;

    private boolean isActive = false;

    private double cur;
    private double prev;
    private long curTimestamp;
    private long sessionId;
    private long sessionStart;

    public TrackingSessionImpl(long userId, LocationTracker tracker, SessionRecorder recorder){
        this.userId = userId;
        this.tracker = tracker;
        this.recorder = recorder;
    }

    void setDistance(double distance) {
        this.prev = this.cur;
        this.cur = distance;
    }

    void setTimestamp(LocationData data){
        this.curTimestamp = data.timestamp();
    }

    void setSessionId(long sessionId){
        this.sessionId = sessionId;
    }

    @Override
    public DailyStat getUpdatedStats(){
        ActivitySession update = getSessionData();
        return recorder.getStartingStat()
                .plus(update);
    }

    @Override
    public long getSessionId() {
        return sessionId;
    }

    @Override
    public long getUserId() {
        return userId;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public Observable<Double> startSession(){
        sessionStart = curTimestamp = System.currentTimeMillis();
        cur = prev = 0;
        return recorder.startRecording(userId)
                .doOnSubscribe(() -> isActive = true)
                .doOnNext(this::setSessionId)
                .flatMap(sessionId -> tracker.startTracking()
                        .flatMap(recorder::recordData)
                        .doOnNext(this::setTimestamp)
                        .compose(ActivitySession::delta)
                        .compose(this::total)
                        .flatMap(this::liveUpdateStats)
                        .doOnNext(this::setDistance)
                ).doOnCompleted(this::release);
    }

    Observable<Double> liveUpdateStats(double distance){
        ActivitySession data = getSessionData();
        return recorder.liveUpdateStats(data)
            .map(succcess -> distance);
    }

    @Override
    public void endSession(){
        tracker.stopTracking();
    }

   void release() {
        isActive = false;
        tracker.release();
        recorder.stopRecording();
    }

    @Override
    public boolean didReachMilestone(){
        double start = recorder
                .getStartingStat()
                .getDistance();
        return calcMilestone(start + prev) < calcMilestone(start + cur);
    }

    @Override
    public int getCurrentMilestone(){
        double starting = recorder
                .getStartingStat()
                .getDistance();
        return calcMilestone(cur + starting);
    }

    @Override
    public ActivitySession getSessionData() {
        return new SessionData.Builder()
                .setSessionId(sessionId)
                .setDistance(cur)
                .setSessionStart(sessionStart)
                .setSessionEnd(curTimestamp)
                .build();
    }

    private int calcMilestone(double distance){
        return ((int)distance/MILESTONE_MULT) * MILESTONE_MULT;
    }


    Observable<Double> total(Observable<Double> obs){
        return obs.map(new Func1<Double, Double>() {
            double total = 0;
            @Override
            public Double call(Double delta) {
                return (total += delta);
            }
        });
    }
}
