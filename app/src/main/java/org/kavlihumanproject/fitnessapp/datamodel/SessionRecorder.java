package org.kavlihumanproject.fitnessapp.datamodel;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;

public interface SessionRecorder {
    boolean isRecording();
    DailyStat getStartingStat();
    Observable<Long> startRecording(long userId);
    Observable<LocationData> recordData(LocationData data);
    void stopRecording();
    Observable<Boolean> liveUpdateStats(ActivitySession session);
}
