package org.kavlihumanproject.fitnessapp.datamodel.data;

public interface LocationData {
    double getLat();
    double getLng();
    long timestamp();
    double distance(LocationData data);
}
