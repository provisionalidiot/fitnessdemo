package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.Activity;
import android.content.IntentSender;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

public class GoogleApiException extends Throwable {

    final int errorCode;
    final boolean hasResolution;
    final Resolution resolution;

    GoogleApiException(ConnectionResult result){
        this(result.getErrorCode(),
                result.hasResolution(),
                result::startResolutionForResult);
    }

    GoogleApiException(Status status){
        this(status.getStatusCode(),
                status.hasResolution(),
                status::startResolutionForResult);
    }

    private GoogleApiException(int errorCode, boolean hasResolution, Resolution resolution) {
        this.errorCode = errorCode;
        this.hasResolution = hasResolution;
        this.resolution = resolution;
    }

    public boolean isResolvable(){
        return hasResolution;
    }

    public int getErrorCode(){
        return errorCode;
    }

    public boolean resolve(Activity activity, int requestCode){
        try {
            resolution.resolve(activity, requestCode);
            return true;
        } catch (IntentSender.SendIntentException e) {
            return false;
        }
    }

    private interface Resolution {
        void resolve(Activity activity, int requestCode)
                throws IntentSender.SendIntentException;
    }
}
