package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

public class CursorOnSubscribe<T> implements Observable.OnSubscribe<T> {

    private final Cursor cursor;
    private final Func1<Cursor, T> factory;

    public CursorOnSubscribe(Cursor cursor, Func1<Cursor, T> factory) {
        this.cursor = cursor;
        this.factory = factory;
    }

    @Override
    public void call(Subscriber<? super T> subscriber) {
        try {
            while (cursor.moveToNext()) {
                if (subscriber.isUnsubscribed()) {
                    break;
                }
                T t = factory.call(cursor);
                if (t != null) {
                    subscriber.onNext(t);
                }
            }

            if (!subscriber.isUnsubscribed()){
                subscriber.onCompleted();
            }

        }catch (SQLiteException e){
          if (!subscriber.isUnsubscribed()){
              subscriber.onError(e);
          }
        } finally {
            cursor.close();
        }
    }
}
