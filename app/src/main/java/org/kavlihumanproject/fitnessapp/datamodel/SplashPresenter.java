package org.kavlihumanproject.fitnessapp.datamodel;



public interface SplashPresenter {
    void checkForActiveUser();
    void cancel();
}
