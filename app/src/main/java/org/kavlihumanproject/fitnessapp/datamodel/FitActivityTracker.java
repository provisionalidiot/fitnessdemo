package org.kavlihumanproject.fitnessapp.datamodel;


import rx.Observable;

public interface FitActivityTracker {
    Observable<Long> listenForActivities(long userId);
    Observable<Boolean> stopListening();
    void release();
}
