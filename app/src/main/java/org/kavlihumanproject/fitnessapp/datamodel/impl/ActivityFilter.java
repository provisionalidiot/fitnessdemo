package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import static com.google.android.gms.location.DetectedActivity.IN_VEHICLE;
import static com.google.android.gms.location.DetectedActivity.ON_BICYCLE;
import static com.google.android.gms.location.DetectedActivity.ON_FOOT;
import static com.google.android.gms.location.DetectedActivity.RUNNING;
import static com.google.android.gms.location.DetectedActivity.STILL;
import static com.google.android.gms.location.DetectedActivity.TILTING;
import static com.google.android.gms.location.DetectedActivity.UNKNOWN;
import static com.google.android.gms.location.DetectedActivity.WALKING;

public class ActivityFilter {

    private static final int PROBABLE = 70;
    private final DetectedActivity activity;

    private static long lastActive;

    public ActivityFilter(DetectedActivity activity){
        this.activity = activity;
    }

    public boolean shouldStart(){
        lastActive = System.currentTimeMillis();
        return isActive() && isLikely();
    }

    public boolean shouldStop(){
        long now = System.currentTimeMillis();
        if (isActive() && isLikely()) {
            lastActive = now;
        }
        return now - lastActive > 60 * 1000 || notActive();
    }

    private boolean notActive(){
        return isLikely() && (activity.getType() == STILL
                || activity.getType() == IN_VEHICLE);
    }

    public String getDescription(){
        switch (activity.getType()) {
            case WALKING:
                return  "walking";
            case ON_BICYCLE:
                return  "on bicycle";
            case ON_FOOT:
                return  "on foot";
            case RUNNING:
                return  "running";
            case STILL:
                return "phone is still";
            case IN_VEHICLE:
                return "driving";
            case UNKNOWN:
                return "unknown";
            case TILTING:
                return "tilting";
            default:
                return "Something else ?";
        }
    }

    private boolean isLikely(){
        return activity.getConfidence() > PROBABLE
                && activity.getType() != UNKNOWN;
    }

    private boolean isActive(){
        int type = activity.getType();
        return type == WALKING || type == ON_FOOT;
    }

    public static  ActivityFilter from(Intent intent){
        if (intent.getBooleanExtra("is_mock", false)){
            return mockActivity(intent);
        }

        DetectedActivity activity = ActivityRecognitionResult
                .extractResult(intent)
                .getMostProbableActivity();
        return new ActivityFilter(activity);
    }

    private static ActivityFilter mockActivity(Intent intent){
        int type = intent.getIntExtra("type", -1);
        DetectedActivity activity = new DetectedActivity(type, 100);
        return new ActivityFilter(activity);
    }

    public static Intent fillMockIntent(Intent intent, int activityType){
        intent.putExtra("is_mock", true);
        intent.putExtra("type", activityType);
        return intent;
    }

    public static boolean isTrackingIntent(Intent intent){
        if (intent.getBooleanExtra("is_mock", false)){
            return true;
        }
        return ActivityRecognitionResult
                .hasResult(intent);
    }
}
