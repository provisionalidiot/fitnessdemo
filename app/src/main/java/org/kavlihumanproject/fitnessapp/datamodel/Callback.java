package org.kavlihumanproject.fitnessapp.datamodel;

public interface Callback<T> {
    void result(T data);
}
