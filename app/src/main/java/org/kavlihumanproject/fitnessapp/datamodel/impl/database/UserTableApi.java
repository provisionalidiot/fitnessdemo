package org.kavlihumanproject.fitnessapp.datamodel.impl.database;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

public class UserTableApi {

    public static final String TABLE_USER_NAME = "users";

    public static final String KEY_PRIMARY = "id";
    public static final String COL_USER_NAME = "username";
    public static final String COL_PW_HASH = "pw_hash";
    public static final String COL_PW_SALT = "pw_salt";
    public static final String COL_DISTANCE = "distance";
    public static final String COL_OFFICE_LAT = "office_lat";
    public static final String COL_OFFICE_LNG = "office_lng";

    private static final String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER_NAME
    + "("+ KEY_PRIMARY +" INTEGER PRIMARY KEY AUTOINCREMENT," + COL_USER_NAME +" TEXT NOT NULL UNIQUE"
            + ", " + COL_PW_HASH + " TEXT NOT NULL, " + COL_PW_SALT + " BLOB NOT NULL"
            + ", " + COL_OFFICE_LAT + " REAL DEFAULT 0.0, " + COL_OFFICE_LNG + " REAL DEFAULT 0.0"
            + ", " + COL_DISTANCE + " INTEGER);";


    private static final String ADD_OFFICE_LAT_COLS = "ALTER TABLE " + TABLE_USER_NAME +
            " ADD COLUMN " + COL_OFFICE_LAT + " REAL DEFAULT 0.0;";


    private static final String ADD_OFFICE_LNG_COLS = "ALTER TABLE " + TABLE_USER_NAME +
            " ADD COLUMN " + COL_OFFICE_LNG + " REAL DEFAULT 0.0;";


    private final SQLiteDatabase db;

    @Inject
    UserTableApi(SQLiteDatabase db) {
        this.db = db;
    }


    void updateSchema(){
        db.execSQL(ADD_OFFICE_LAT_COLS);
        db.execSQL(ADD_OFFICE_LNG_COLS);
    }

    void create(){
        db.execSQL(CREATE_USER_TABLE);
    }

    void drop(){
        String drop = "DROP TABLE " + TABLE_USER_NAME + " IF EXISTS;";
        db.execSQL(drop);
    }

    public Cursor getUserByName(String username){
        DbHelper.assertRunningOnTheDbScheduler();
        final String[] columns = {KEY_PRIMARY, COL_USER_NAME, COL_PW_HASH,
                COL_PW_SALT, COL_OFFICE_LAT, COL_OFFICE_LNG};
        final String where = COL_USER_NAME + " = ? COLLATE NOCASE";
        final String [] whereArgs = {username};
        return db.query(TABLE_USER_NAME, columns, where,
                whereArgs, null, null, null);
    }

    public long createUser(String username, String hash, byte[] salt){
        DbHelper.assertRunningOnTheDbScheduler();
        ContentValues values = new ContentValues();
        values.put(COL_USER_NAME, username);
        values.put(COL_PW_HASH, hash);
        values.put(COL_PW_SALT, salt);
        return db.insertOrThrow(TABLE_USER_NAME, null, values);
    }

    public Cursor getLeaderBoardAllTime(){
        DbHelper.assertRunningOnTheDbScheduler();
        final String[] columns = {KEY_PRIMARY, COL_USER_NAME, COL_DISTANCE};
        final String orderBy = COL_DISTANCE + " DESC";
        final String limit = "25";
        return db.query(TABLE_USER_NAME, columns, null, null,
                null, null, orderBy, limit);
    }

    public Cursor getAllUsers(){
        DbHelper.assertRunningOnTheDbScheduler();
        final String[] columns = {KEY_PRIMARY, COL_USER_NAME, COL_DISTANCE};
        return db.query(TABLE_USER_NAME, columns, null, null,
                null, null, null);
    }

    public boolean updateDistance(long userId, int distance){
        DbHelper.assertRunningOnTheDbScheduler();
        ContentValues values = new ContentValues();
        values.put(COL_DISTANCE, distance);
        final String where = KEY_PRIMARY + " = " + userId;
        return db.update(TABLE_USER_NAME, values, where, null) != -1;
    }

    public boolean updateOfficeLocation(long userId, double lat, double lng){
        DbHelper.assertRunningOnTheDbScheduler();
        ContentValues values = new ContentValues();
        values.put(COL_OFFICE_LAT, lat);
        values.put(COL_OFFICE_LNG, lng);
        final String where = KEY_PRIMARY + " = " + userId;
        return db.update(TABLE_USER_NAME, values, where, null) != -1;
    }

    public static long getId(Cursor cursor){
        int idIndex = cursor.getColumnIndex(KEY_PRIMARY);
        return cursor.getLong(idIndex);
    }

    public static String getUsername(Cursor cursor){
        int nameIndex = cursor.getColumnIndex(COL_USER_NAME);
        return cursor.getString(nameIndex);
    }

    public static String getHash(Cursor cursor){
        int hashIndex = cursor.getColumnIndex(COL_PW_HASH);
        return cursor.getString(hashIndex);
    }

    public static byte[] getSalt(Cursor cursor){
        int saltIndex = cursor.getColumnIndex(COL_PW_SALT);
        return cursor.getBlob(saltIndex);
    }

    public static int getDistance(Cursor cursor){
        int distanceIndex = cursor.getColumnIndex(COL_DISTANCE);
        return cursor.getInt(distanceIndex);
    }

    public static double getOfficeLat(Cursor cursor){
        int latIndex = cursor.getColumnIndex(COL_OFFICE_LAT);
        return cursor.getDouble(latIndex);
    }
    public static double getOfficeLng(Cursor cursor){
        int lngIndex = cursor.getColumnIndex(COL_OFFICE_LNG);
        return cursor.getDouble(lngIndex);
    }

}
