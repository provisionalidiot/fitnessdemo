package org.kavlihumanproject.fitnessapp.datamodel.impl;


import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.ui.model.DailyStatsModel;
import org.kavlihumanproject.fitnessapp.util.RxLogger;

import rx.Subscription;

public class LiveStatsPresenter {
    private final DailyStatsModel model;
    private final LiveUpdatesProvider provider;
    private Subscription subscription;

    public LiveStatsPresenter(DailyStatsModel model, LiveUpdatesProvider provider) {
        this.model = model;
        this.provider = provider;
    }

    public void listenForLiveUpdates(){
        subscription = provider.getLiveUpdates()
                .map(new RxLogger<>("LiveUpdate"))
                .subscribe(this::onLiveUpdate,
                        this::handleError);
    }

    void onLiveUpdate(DailyStat update){
       int index = -1;
        for (int i = 0; i < model.items.size(); i++){
            DailyStat stat = model.items.get(i);
            if (stat.getId() == update.getId()){
                index = i;
                break;
            }
        }

        if (index > -1){
            model.items.set(index, update);
        }else {
            model.items.add(0, update);
        }
    }

    void handleError(Throwable t){
        t.printStackTrace();
    }

    public void release(){
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }
}
