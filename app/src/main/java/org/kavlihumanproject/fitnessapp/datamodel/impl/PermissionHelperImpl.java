package org.kavlihumanproject.fitnessapp.datamodel.impl;


import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import org.kavlihumanproject.fitnessapp.datamodel.AppException;
import org.kavlihumanproject.fitnessapp.datamodel.PermissionHelper;
import org.kavlihumanproject.fitnessapp.util.StringsUtil;

import java.util.Set;

import rx.Observable;
import rx.subjects.AsyncSubject;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

class PermissionHelperImpl implements PermissionHelper {

    private static final Set<String> VALID_PERMISSIONS
            = StringsUtil.asSet(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION});

    private static final int PERMISSION_REQUEST = 411;

    private final Activity activity;
    private final AsyncSubject<Boolean> permListener;
    private final String permission;

    PermissionHelperImpl(Activity activity, String permission) {
        assertValidPermission(permission);
        this.activity = activity;
        this.permListener = AsyncSubject.create();
        this.permission = permission;
    }

    private void assertValidPermission(String permission){
        if (!VALID_PERMISSIONS.contains(permission))
            throw new AssertionError(permission + " is not a valid permission");
    }

    private void assertNotCompleted(){
        if (permListener.hasCompleted())
            throw new IllegalStateException("Permission Helper is one time use only");
    }

    @Override
    public boolean isReleased() {
        return permListener.hasCompleted();
    }

    @Override
    public final boolean hasPermission() {
        int result = ContextCompat.checkSelfPermission(activity,
                permission);
        return PERMISSION_GRANTED == result;
    }

    @Override
    public final <T> Observable<T> requestPermission(T t) {
        assertNotCompleted();

        if (hasPermission()) {
            return Observable.just(t);
        }

        Observable<T> tObs = permListener
                .concatMap(granted -> granted
                        ? Observable.just(t)
                        : Observable.error(new AppException(1))
                );

        requestPermission();

        return tObs;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(activity,
                new String[]{permission},
                PERMISSION_REQUEST);
    }

    @Override
    public final void handleResult(boolean granted) {
        if (!permListener.hasCompleted()) {
            permListener.onNext(granted);
            permListener.onCompleted();
        }
    }

    @Override
    public final void cancelRequest() {
        if (!permListener.hasCompleted())
            permListener.onError(new AppException(1));
    }


    @Override
    public void handleRequestResults(int requestCode, String[] perms, int[] results){
        if (requestCode != PERMISSION_REQUEST) {
            cancelRequest();
            return;
        }

        boolean granted = false;
        for (int i = 0, n = Math.min(perms.length, results.length); i < n; i++){
            if (permission.equals(perms[i])
                    && results[i] == PERMISSION_GRANTED) {
                granted = true;
                break;
            }
        }

        handleResult(granted);
    }
}
