package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.datamodel.NotificationPresenter;

public class NotificationPresenterImpl implements NotificationPresenter {

    private static final int MILESTONE_ID = 1231;
    private static final int REMINDER_ID = 1232;
    private static final int ACTIVITY_ID = 1233;


    private final Context context;
    private final NotificationManager manager;


    public NotificationPresenterImpl(Context context, NotificationManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void showMilestoneNotification(int milestone) {
        Resources resources = context.getResources();
        String message = resources.getString(R.string.milestone, milestone);
        String title = resources.getString(R.string.milstone_title);
        showMessage(title, message, MILESTONE_ID);
    }

    @Override
    public void showReminderNotificaion() {
        Resources resources = context.getResources();
        String message = resources.getString(R.string.reminder);
        String title = resources.getString(R.string.reminder_title);
        showMessage(title, message, REMINDER_ID);
    }

    public void showMessage(String message){
        showMessage("Activity tracking service", message, ACTIVITY_ID);
    }

    private void showMessage(String title, String message, int id){
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_light)
                .setLights(Color.argb(255,0,0,255), 100, 250)
                .setVibrate(new long[] { 0, 250})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .build();
        manager.notify(id, notification);
    }
}
