package org.kavlihumanproject.fitnessapp.datamodel.impl;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

public class RetryWithDelay implements Func1<Observable<? extends Throwable>, Observable<?>> {

    private final int maxTries;
    private final int delayInSeconds;
    private int retries;

    public RetryWithDelay(int maxTries, int delayInSeconds) {
        this.maxTries = maxTries;
        this.delayInSeconds = delayInSeconds;
    }

    @Override
    public Observable<?> call(Observable<? extends Throwable> observable) {
        return observable.flatMap(throwable -> {
            if (throwable instanceof GoogleApiException) {
                return Observable.error(throwable);
            }

            if (++retries < maxTries){
                return Observable.timer(delayInSeconds,
                        TimeUnit.SECONDS);
            }

            return Observable.error(throwable);

        });
    }
}
