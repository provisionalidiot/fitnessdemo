package org.kavlihumanproject.fitnessapp.datamodel.impl;

import android.database.Cursor;
import android.util.Log;

import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.util.RxLogger;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class GeofenceBootRegistrar {

    private final LocalUserCache cache;
    private final GeofencingClient client;
    private final UserTableApi userApi;
    private final Scheduler scheduler;

    public GeofenceBootRegistrar(LocalUserCache cache, GeofencingClient client,
                                 UserTableApi userApi, Scheduler scheduler) {
        this.cache = cache;
        this.client = client;
        this.userApi = userApi;
        this.scheduler = scheduler;
    }

    public Observable<Boolean> register(){
        return cache.getActiveUser()
                .map(new RxLogger<>("active user"))
                .flatMap(user -> {

                    Log.e("ll", "user is present? " + user.isPresent());
                    if (user.isPresent()){
                        return getOfficeLocation(user.get())
                                .flatMap(client::registerGeofence)
                                .map(new RxLogger<>("register geofence"))
                                .flatMap(this::forceGeofenceUpdates);
                    }
                    return Observable.just(false);
                });
    }

    Observable<Boolean> forceGeofenceUpdates(boolean success){
        if (success){
            client.getCurrentLocation()
                    .map(data -> true);
        }
        return Observable.just(false);
    }

    Observable<GeofencingClient.GeoFence> getOfficeLocation(LocalUser user){
        return Observable.just(user.getUsername())
                .map(new RxLogger<>("username"))
                .map(this::officeLocation)
                .map(new RxLogger<>("office coords"))
                .subscribeOn(scheduler)
                .observeOn(Schedulers.io())
                .map(GeofencingClient::convertToGeofence);
    }

    LocationData officeLocation(String username){
        Cursor cursor = userApi.getUserByName(username);
        if (!cursor.moveToFirst()){
            throw new RuntimeException("User is missing from database!");
        }

        double lat = UserTableApi.getOfficeLat(cursor);
        double lng = UserTableApi.getOfficeLng(cursor);
        return new SpaceTime(lat, lng, 0);
    }

    public void release() {
        client.release();
    }
}
