package org.kavlihumanproject.fitnessapp.datamodel.data;


public interface DailyStat {

    long getId();
    String getDate();
    int getSessions();
    double getDistance();
    double getDuration();
    DailyStat plus(ActivitySession session);
}
