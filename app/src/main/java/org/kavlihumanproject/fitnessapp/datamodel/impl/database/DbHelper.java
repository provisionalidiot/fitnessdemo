package org.kavlihumanproject.fitnessapp.datamodel.impl.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Scheduler;
import rx.schedulers.Schedulers;

public class DbHelper extends SQLiteOpenHelper {

    private static final int CURRENT_DB_VERSION = 2;
    private static final String DB_NAME = "fitness_app.db";
    private static final Scheduler DB_SCHEDULER;
    private static final String DB_THREAD = DbHelper.class.getCanonicalName()+"-thread";

    static {
        ExecutorService executor = Executors
                .newSingleThreadExecutor(r -> new Thread(r, DB_THREAD));
        DB_SCHEDULER = Schedulers.from(executor);
    }

    private static DbHelper singleton;

    private DbHelper(Context context) {
        super(context, DB_NAME, null, CURRENT_DB_VERSION);
    }


    public UserTableApi getUserTableApi(){
        return new UserTableApi(getWritableDatabase());
    }

    public SessionDataApi getSessionDataApi(){
        return new SessionDataApi(getWritableDatabase());
    }

    public DailyStatsApi getDailyStatsApi(){
        return new DailyStatsApi(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       getScheduler().createWorker().schedule(() -> {
           try {
               sqLiteDatabase.beginTransaction();
               new UserTableApi(sqLiteDatabase).create();
               new SessionDataApi(sqLiteDatabase).create(); /*ActivityDataTable contains foreign keys so must come after UserTableApi.create*/
               new DailyStatsApi(sqLiteDatabase).create();
               sqLiteDatabase.setTransactionSuccessful();
           }finally {
               sqLiteDatabase.endTransaction();
           }
       });
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion ==  CURRENT_DB_VERSION) {
            getScheduler().createWorker().schedule(() -> {
                try {
                    sqLiteDatabase.beginTransaction();
                    new UserTableApi(sqLiteDatabase).updateSchema();
                } finally {
                    sqLiteDatabase.endTransaction();
                }
            });
        }
    }

    @Override
    public synchronized void close() {
        throw new RuntimeException("Shouldn't call close");
    }

    public Scheduler getScheduler(){
        return DB_SCHEDULER;
    }

    public static void assertRunningOnTheDbScheduler(){
        if (!DB_THREAD.equals(Thread.currentThread().getName())) {
            throw new AssertionError("Database queries should be run via" +
                    " the DbHelper Scheduler (see DbHelper.getScheduler()).");
        }
    }

    public static synchronized DbHelper getHelper(Context context){
        if (singleton == null)
            singleton = new DbHelper(context.getApplicationContext());

        return singleton;
    }
}
