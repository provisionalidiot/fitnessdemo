package org.kavlihumanproject.fitnessapp.util.testing;

import org.kavlihumanproject.fitnessapp.datamodel.SessionRecorder;
import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import rx.Observable;

public class EmptySessionRecorder implements SessionRecorder {

    private final int startingDistance;
    private final int startingDuration;

    public EmptySessionRecorder(int startingDistance, int startingDuration) {
        this.startingDistance = startingDistance;
        this.startingDuration = startingDuration;
    }

    @Override
    public boolean isRecording() {
        return true;
    }

    @Override
    public DailyStat getStartingStat() {
        return new MockStartingSession(startingDistance,startingDuration);
    }

    @Override
    public Observable<Long> startRecording(long userId) {
        return Observable.just(0L);
    }

    @Override
    public Observable<LocationData> recordData(LocationData data) {
        return Observable.just(data);
    }

    @Override
    public void stopRecording() {

    }

    @Override
    public Observable<Boolean> liveUpdateStats(ActivitySession session) {
        return Observable.just(true);
    }


    private static class MockStartingSession implements DailyStat {

        private final double distance;
        private final double duration;

        private MockStartingSession(double distance, double duration) {
            this.distance = distance;
            this.duration = duration;
        }

        @Override
        public long getId() {
            return 1;
        }

        @Override
        public String getDate() {
            return "";
        }

        @Override
        public int getSessions() {
            return 0;
        }

        @Override
        public double getDistance() {
            return distance;
        }

        @Override
        public double getDuration() {
            return duration;
        }

        @Override
        public DailyStat plus(ActivitySession session) {
            return new MockStartingSession(distance + session.getDistance(),
                    duration + session.getDuration());
        }
    }
}
