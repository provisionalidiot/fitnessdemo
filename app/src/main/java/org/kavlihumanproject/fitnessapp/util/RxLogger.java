package org.kavlihumanproject.fitnessapp.util;

import android.util.Log;

import rx.functions.Func1;

public class RxLogger<T> implements Func1<T, T> {

    private final String message;

    public RxLogger(String message) {
        this.message = message;
    }

    @Override
    public T call(T t) {
        Log.e("RxLogger", message+" : " + t);
        return t;
    }
}
