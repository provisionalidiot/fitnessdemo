package org.kavlihumanproject.fitnessapp.util.testing;

import org.kavlihumanproject.fitnessapp.datamodel.SessionRecorder;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.SessionDataApi;
import org.kavlihumanproject.fitnessapp.tasks.TrackingSessionModule;

import rx.Scheduler;

public class MockTrackingSessionModule extends TrackingSessionModule {

    public MockTrackingSessionModule(long userId) {
        super(userId);
    }

    @Override
    public SessionRecorder provideRecorder(SessionDataApi sessionApi, DailyStatsApi statsApi, Scheduler scheduler) {
        return new EmptySessionRecorder(0, 0);
    }
}
