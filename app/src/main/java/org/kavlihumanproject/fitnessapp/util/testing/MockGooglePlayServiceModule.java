package org.kavlihumanproject.fitnessapp.util.testing;

import android.content.Context;
import android.os.Handler;

import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GoogleApiProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;

public class MockGooglePlayServiceModule extends GooglePlayServicesModule {

    public MockGooglePlayServiceModule(Context context) {
        super(context);
    }

    @Override
    public Handler provideHandler() {
        return new Handler();
    }

    @Override
    public GoogleApiProvider providerGoogleApi() {
        return super.providerGoogleApi();
    }

    @Override
    public FitActivityTracker provideFitActivityTracker(GoogleApiProvider googleApiProvider) {
        return new EmptyActivityTracker();
    }

    @Override
    public LocationTracker provideLocationTracker(GoogleApiProvider provider, Handler handler) {
        return new DelayedMockLocationTracker(1000, 1000);
    }
}
