package org.kavlihumanproject.fitnessapp.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtil {

    private static final SimpleDateFormat DATE_FORMAT;
    private static final SimpleDateFormat DAY_FORMAT;

    public static final double ONE_HOUR = 1000.0 * 60 * 60;
    public static final long ONE_DAY = 86400000L;

    static {
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        DATE_FORMAT.setTimeZone(TimeZone.getDefault());

        DAY_FORMAT = new SimpleDateFormat("MMM d, yyyy", Locale.US);
        DAY_FORMAT.setTimeZone(TimeZone.getDefault());
    }

    public static String print(long timestamp){
        return DATE_FORMAT.format(new Date(timestamp));
    }

    public static String printDay(long timestamp){
        return DAY_FORMAT.format(new Date(timestamp));
    }


    public static void assertDateTimestamp(long date){
        if (date != getToday(date)){
            throw new AssertionError("This date (" + print(date) +
                     ") is being use to query the database" +
                    " and therefore must be EXACTLY 12:00 am");
        }
    }

    public static long getToday(){
        return getToday(System.currentTimeMillis());
    }

    public static long getToday(long timestamp){
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(timestamp);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        calendar.clear();
        calendar.set(year, month, day);

        return calendar.getTimeInMillis();
    }

    public static long getTomorrow(long timestamp){
        return getToday(timestamp + ONE_DAY);
    }

    public static long getYesterday(long timestamp){
        return getToday(timestamp - ONE_DAY);
    }

    public static double toHours(long duration){
        return ((double) duration) / ONE_HOUR;
    }
}
