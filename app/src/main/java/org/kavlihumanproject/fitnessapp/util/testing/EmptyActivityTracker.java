package org.kavlihumanproject.fitnessapp.util.testing;

import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;

import rx.Observable;

public class EmptyActivityTracker implements FitActivityTracker {

    @Override
    public Observable<Long> listenForActivities(long userId) {
        return Observable.just(1L);
    }

    @Override
    public Observable<Boolean> stopListening() {
        return Observable.just(true);
    }

    @Override
    public void release() {

    }
}
