package org.kavlihumanproject.fitnessapp.util.testing;


import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import java.util.concurrent.TimeUnit;

import rx.Observable;

public class DelayedMockLocationTracker extends MockLocationTracker {

    public DelayedMockLocationTracker(int distance, long duration) {
        super(distance, duration);
    }

    @Override
    public Observable<LocationData> startTracking() {
        return super.startTracking().flatMap(location ->
                Observable.just(location)
                    .delay(1500, TimeUnit.MILLISECONDS));
    }
}
