package org.kavlihumanproject.fitnessapp.util.testing;

import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;
import org.kavlihumanproject.fitnessapp.datamodel.impl.TrackingSessionImpl;

public class TestingUtil {

    public static TrackingSession createTrackingSession(int sessionDistance, int startingDist, int startingDur, long duration) {
        return new TrackingSessionImpl(0L, new MockLocationTracker(sessionDistance, duration),
                new EmptySessionRecorder(startingDist, startingDur));
    }


    public static TrackingSession createDelayedTrackingSession() {
        return new TrackingSessionImpl(0L, new DelayedMockLocationTracker(1000, 1000),
                new EmptySessionRecorder(0, 0));
    }
}
