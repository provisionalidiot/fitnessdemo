package org.kavlihumanproject.fitnessapp.util;

import android.support.annotation.NonNull;

import rx.functions.Func1;


public class Optional<T>{
    private final T data;

    private Optional(T data) {
        this.data = data;
    }

    private Optional(){
        data = null;
    }

    public T get(){
        if (data == null){
            throw new IllegalAccessError("Optional was empty");
        }

        return data;
    }

    public boolean isPresent(){
        return data != null;
    }

    public <U> Optional<U> map(@NonNull Func1<T, U> func){
        if (data == null){
            return empty();
        }

        return of(func.call(data));
    }

    public <U> Optional<U> flatMap(@NonNull Func1<T, Optional<U>> func){
        if (data == null){
            return empty();
        }
        return func.call(data);
    }

    public static <U> Optional<U> empty(){
        return new Optional<>();
    }

    public static <U> Optional<U> of(@NonNull  U data){
        return new Optional<U>(data);
    }

}
