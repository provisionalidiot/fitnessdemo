package org.kavlihumanproject.fitnessapp.util.testing;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocationData;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class MockLocationTracker implements LocationTracker {

    private static final GeodeticCalculator GEO_CALC
            = new GeodeticCalculator();

    private List<LocationData> mockData;
    private final int total;
    private final long duration;

    public MockLocationTracker(int distance, long duration){
        this.total = distance;
        this.duration = duration;
        createMockData();
    }

    private void createMockData(){
        mockData = new ArrayList<>();
        GeodeticCalculator geoCalc = new GeodeticCalculator();

        Ellipsoid reference = Ellipsoid.WGS84;
        GlobalCoordinates startingCoords;
        startingCoords = new GlobalCoordinates( 38.88922, -77.04978 );
        double startBearing = 51.7679;
        double distance = 1.0001;

        long startingTimeStamp = System.currentTimeMillis();

        Data data = new Data(startingCoords, startingTimeStamp);
        for (int i = 0; i < total; i++) {
            mockData.add(data);
            double[] endBearing = new double[1];
            GlobalCoordinates dest = geoCalc.calculateEndingGlobalCoordinates(
                    reference, startingCoords, startBearing, distance, endBearing);
            startingCoords = dest;
            data = new Data(dest, startingTimeStamp + duration);
        }

        // should 1000 meters, 500/60 minutes
    }

    @Override
    public Observable<LocationData> startTracking() {
        return Observable.from(mockData);
    }

    @Override
    public void stopTracking() {

    }

    @Override
    public void release() {

    }

    private static class Data implements LocationData {
        private final GlobalCoordinates coordinates;
        private final long timestamp;

        private Data(GlobalCoordinates coordinates, long timestamp) {
            this.coordinates = coordinates;
            this.timestamp = timestamp;
        }

        @Override
        public double getLat() {
            return coordinates.getLatitude();
        }

        @Override
        public double getLng() {
            return coordinates.getLongitude();
        }

        @Override
        public long timestamp() {
            return timestamp;
        }

        @Override
        public double distance(LocationData data) {
            GlobalCoordinates dataCoordinates = new GlobalCoordinates(data.getLat(), data.getLng());
            return GEO_CALC.calculateGeodeticCurve(Ellipsoid.WGS84, coordinates, dataCoordinates)  /* meters */
                    .getEllipsoidalDistance();
        }
    }
}
