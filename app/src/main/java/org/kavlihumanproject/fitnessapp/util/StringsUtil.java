package org.kavlihumanproject.fitnessapp.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class StringsUtil {
    private StringsUtil(){}

    public static boolean isEmpty(String str){
        return str == null || str.isEmpty();
    }

    public static Set<String> asSet(String [] array){
        HashSet<String> set =  new HashSet<>(Arrays.asList(array));
        return Collections.unmodifiableSet(set);
    }

}
