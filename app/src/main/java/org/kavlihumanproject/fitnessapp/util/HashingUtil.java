package org.kavlihumanproject.fitnessapp.util;


import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class HashingUtil {
    private static final SecureRandom RAND = new SecureRandom();
    private static final int SALT_LEN = 15;

    public static String sha1Hash(@NonNull String string, @NonNull byte[] salt) {
        try {
            byte[] bytes = new byte[string.length() + salt.length];
            System.arraycopy(string.getBytes("UTF-8"), 0, bytes, 0, string.length());
            System.arraycopy(salt, 0, bytes, string.length(), salt.length);
            MessageDigest digest = MessageDigest.getInstance( "SHA-256" );
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            return bytesToHex(bytes);
        } catch( NoSuchAlgorithmException | UnsupportedEncodingException e ) {
            throw new RuntimeException("Shouldn't happen, as android explicitly provides these formats");
        }
    }

    public static byte[] generateSalt(){
        byte[] salt = new byte[SALT_LEN];
        RAND.nextBytes(salt);
        return salt;
    }

    private static String bytesToHex(byte [] bytes){
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for(byte b: bytes) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }
}
