package org.kavlihumanproject.fitnessapp;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GeofenceBootRegistrar;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import rx.Scheduler;

@Module(includes = GooglePlayServicesModule.class)
public class BootReceiverModule {
    @Provides @ContextScope
    GeofenceBootRegistrar provideBootRegistrar(LocalUserCache cache, UserTableApi api,
                                               Scheduler scheduler, GeofencingClient client){
        return new GeofenceBootRegistrar(cache, client, api, scheduler);
    }

    @Subcomponent(modules = BootReceiverModule.class)
    @ContextScope
    public interface BootReceiverComponent {
        GeofenceBootRegistrar getRegistrar();
        @Subcomponent.Builder
        interface Builder{
            BootReceiverComponent.Builder playServicesModule(GooglePlayServicesModule module);
            BootReceiverComponent.Builder bootReceiverModule(BootReceiverModule module);
            BootReceiverComponent build();
        }
    }
}
