package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.FragmentScope;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LeaderboardPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LocalUserDbProviderImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.ui.model.LeaderboardModel;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public class LeaderboardModule {

    private final LeaderboardModel model;

    public LeaderboardModule(LeaderboardModel model) {
        this.model = model;
    }

    @Provides @FragmentScope(LeaderboardFragment.class)
    LocalUserProvider provideUserProvider(UserTableApi userApi, DailyStatsApi statsApi, Scheduler scheduler){
        return new LocalUserDbProviderImpl(userApi, statsApi, scheduler);
    }

    @Provides @FragmentScope(LeaderboardFragment.class)
    LeaderboardPresenter provideLeaderBoardPresenter(LocalUserProvider provider){
        return new LeaderboardPresenter(model, provider);
    }
}
