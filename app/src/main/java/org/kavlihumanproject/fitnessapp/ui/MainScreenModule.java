package org.kavlihumanproject.fitnessapp.ui;

import android.content.Intent;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.impl.ActivityNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.impl.User;

import dagger.Module;
import dagger.Provides;

import static org.kavlihumanproject.fitnessapp.ui.MainActivity.KEY_USER_ID;
import static org.kavlihumanproject.fitnessapp.ui.MainActivity.KEY_USER_NAME;

@Module(
        subcomponents = {
                DailyStatsComponent.class,
                LeaderBoardComponent.class
        }
)
public class MainScreenModule {
    private final MainActivity activity;

    public MainScreenModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ContextScope
    ScreenNavigator provideNavigator() {
        return new ActivityNavigator(activity);
    }

    @Provides
    @ContextScope
    LocalUser provideUser() {
        Intent intent = activity.getIntent();
        long id = intent.getLongExtra(KEY_USER_ID, -1);
        String username = intent.getStringExtra(KEY_USER_NAME);
        return new User(id, username);
    }
}
