package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;

import dagger.Subcomponent;

@Subcomponent(modules = SplashScreenModule.class)
@ContextScope
public interface SplashComponent {
    void inject(SplashActivity activity);

    @Subcomponent.Builder
    interface Builder {
        SplashComponent.Builder setSplashScreenModule(SplashScreenModule module);
        SplashComponent.Builder setPlayServicesModule(GooglePlayServicesModule module);
        SplashComponent build();
    }
}
