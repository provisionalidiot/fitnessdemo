package org.kavlihumanproject.fitnessapp.ui;


import android.os.Bundle;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.LoginPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.impl.ActivityNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LocalUserDbProviderImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LoginPresenterImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.ui.model.LoginScreenModel;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

import static org.kavlihumanproject.fitnessapp.ui.LoginScreenActivity.BUNDLE_KEY_MODEL;

@Module(includes = GooglePlayServicesModule.class)
public class LoginScreenModule {

    private final LoginScreenActivity activity;
    private final Bundle saved;

    public LoginScreenModule(LoginScreenActivity activity, Bundle saved) {
        this.activity = activity;
        this.saved = saved;
    }

    @Provides @ContextScope
    LoginScreenModel provideModel(){
        if (saved == null) {
            return new LoginScreenModel();
        }else {
            return saved.getParcelable(BUNDLE_KEY_MODEL);
        }
    }

    @Provides @ContextScope
    LocalUserProvider providerUserProvider(UserTableApi userApi, DailyStatsApi statsApi, Scheduler scheduler){
        return new LocalUserDbProviderImpl(userApi, statsApi, scheduler);
    }

    @Provides @ContextScope
    ScreenNavigator provideScreenNavigator(){
        return new ActivityNavigator(activity);
    }

    @Provides @ContextScope
    LoginPresenter providePresenter(LoginScreenModel model, LocalUserProvider userProvider,
                                    LocalUserCache userCache, ScreenNavigator navigator, GeofencingClient client){

        return new LoginPresenterImpl(model, userProvider, client, navigator, userCache);
    }
}
