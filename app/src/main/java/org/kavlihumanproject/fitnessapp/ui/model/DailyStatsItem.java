package org.kavlihumanproject.fitnessapp.ui.model;

import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.widget.TextView;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import java.util.Locale;

public class DailyStatsItem implements DailyStat {
    private final long id;
    private final long date;
    private final int sessions;
    private final double distance;
    private final double duration;
    private final String monthAndDay;

    public DailyStatsItem(long id, long date, int session, double distance, double duration){
        this.id = id;
        this.date = date;
        this.sessions = session;
        this.distance = distance;
        this.duration = duration;
        this.monthAndDay = TimeUtil.printDay(date);
    }

    public DailyStatsItem(long id, long date){
        this.id = id;
        this.date = date;
        this.sessions = 0;
        this.distance = 0;
        this.duration = 0;
        this.monthAndDay = TimeUtil.printDay(date);
    }

    @Override
    public long getId() {
        return id;
    }

    public String getDate() {
        return monthAndDay;
    }

    public int getSessions() {
        return sessions;
    }

    public double getDistance() {
        return distance;
    }

    public double getDuration() {
        return duration;
    }

    @BindingAdapter({"app:content"})
    public static void getContent(TextView view, DailyStat item) {
        Resources resources = view.getContext().getResources();
        view.setText(getContent(resources, item));
    }

    private static CharSequence getContent(Resources resources, DailyStat item){
        SpannableStringBuilder sb = new SpannableStringBuilder();
        nextSpan(sb, resources.getString(R.string.daily_stat_item_label_session));
        sb.append(" ");
        sb.append(String.valueOf(item.getSessions()));
        sb.append("   ");
        nextSpan(sb, resources.getString(R.string.daily_stat_item_label_distance));
        sb.append(" ");
        sb.append(String.valueOf((int) item.getDistance()));
        sb.append("   ");
        nextSpan(sb, resources.getString(R.string.daily_stat_item_label_duration));
        sb.append(" ");
        sb.append(String.format(Locale.getDefault(), "%.2f", item.getDuration()));
        sb.append(" ");
        sb.append(resources.getString(R.string.daily_stat_item_label_hours));
        return sb;
    }

    private static void nextSpan(SpannableStringBuilder sb, String string){
        int start = sb.length();
        sb.append(string);
        sb.setSpan(new StyleSpan(Typeface.BOLD),
                start, start + string.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    }

    public DailyStatsItem plus(ActivitySession session){
        return new DailyStatsItem(id, date, sessions + 1, distance + session.getDistance(),
                duration + TimeUtil.toHours(session.getDuration()));
    }

    @Override
    public String toString() {
        return "id " + id + " # of Sessions: " + String.valueOf(sessions) +
                ", " + "Distance Walked: " + String.valueOf((int) distance) + ", " +
                "Active: " + String.format(Locale.getDefault(), "%.2f", duration) + " hours";
    }
}
