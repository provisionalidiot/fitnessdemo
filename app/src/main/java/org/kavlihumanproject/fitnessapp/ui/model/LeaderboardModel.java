package org.kavlihumanproject.fitnessapp.ui.model;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.widget.TextView;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.LeaderboardAdapter;

import java.util.List;

public class LeaderboardModel {
    public final ObservableArrayList<LocalUser> leaderboard = new ObservableArrayList<>();

    public LeaderboardModel() {

    }

    @BindingAdapter("app:items")
    public static void setAdapter(RecyclerView view, List<LocalUser> users){
        view.setAdapter(new LeaderboardAdapter(users));
    }


    @BindingAdapter({"app:content"})
    public static void getContent(TextView view, LocalUser user) {
        SpannableStringBuilder sb = new SpannableStringBuilder();
        nextSpan(sb, user.getUsername());
        sb.append("  ");
        sb.append(String.valueOf((int) user.getDistance()));
        sb.append(" meters");
        view.setText(sb);
    }

    private static void nextSpan(SpannableStringBuilder sb, String string){
        int start = sb.length();
        sb.append(string);
        sb.setSpan(new StyleSpan(Typeface.BOLD),
                start, start + string.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    }

}
