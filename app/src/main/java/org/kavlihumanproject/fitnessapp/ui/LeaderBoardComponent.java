package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.FragmentScope;

import dagger.Subcomponent;

@Subcomponent(modules = {LeaderboardModule.class})
@FragmentScope(LeaderboardFragment.class)
public interface LeaderBoardComponent {
    void inject(LeaderboardFragment fragment);
    @Subcomponent.Builder
    interface Builder {
        LeaderBoardComponent.Builder module(LeaderboardModule module);
        LeaderBoardComponent build();
    }
}
