package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.PermissionHelper;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.SplashPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.ActivityNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LocationPermissionHelper;
import org.kavlihumanproject.fitnessapp.datamodel.impl.SplashPresenterImpl;
import org.kavlihumanproject.fitnessapp.ui.model.SplashScreenModel;

import dagger.Module;
import dagger.Provides;

@Module(includes = GooglePlayServicesModule.class)
public class SplashScreenModule {

    private final SplashActivity splashActivity;

    public SplashScreenModule(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Provides @ContextScope
    SplashScreenModel provideModel(){
        return new SplashScreenModel();
    }

    @Provides @ContextScope
    PermissionHelper providePremissionHelper(){
        return new LocationPermissionHelper(splashActivity);
    }


    @Provides @ContextScope
    ScreenNavigator provideScreenNavigator(){
        return new ActivityNavigator(splashActivity);
    }

    @Provides @ContextScope
    SplashPresenter providePresenter(ScreenNavigator screenNavigator, LocalUserCache cache,
                                     PermissionHelper helper, FitActivityTracker tracker, SplashScreenModel model){
        return new SplashPresenterImpl(screenNavigator, cache, helper, tracker, model);
    }
}
