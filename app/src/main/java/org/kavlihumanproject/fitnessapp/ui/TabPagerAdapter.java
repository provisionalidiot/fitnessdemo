package org.kavlihumanproject.fitnessapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;

public class TabPagerAdapter extends FragmentStatePagerAdapter {

    private final LocalUser user;

    public TabPagerAdapter(FragmentManager fm, LocalUser user) {
        super(fm);
        this.user = user;
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0
                ? DailyStatsFragment.newInstance(user)
                : new LeaderboardFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? "Daily Statistics" : "Leaderboard";
    }
}
