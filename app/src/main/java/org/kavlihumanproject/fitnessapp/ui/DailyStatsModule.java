package org.kavlihumanproject.fitnessapp.ui;

import android.content.Context;

import org.kavlihumanproject.fitnessapp.dagger.FragmentScope;
import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient;
import org.kavlihumanproject.fitnessapp.datamodel.LocalUserCache;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.impl.DailyStatsPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.DailyStatsProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LiveStatsPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LiveUpdatesProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.UserTableApi;
import org.kavlihumanproject.fitnessapp.ui.model.DailyStatsModel;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module(includes = GooglePlayServicesModule.class)
public class DailyStatsModule {

    private final Context context;
    private final DailyStatsModel model;

    public DailyStatsModule(Context context, DailyStatsModel model) {
        this.context = context;
        this.model = model;
    }

    @Provides @FragmentScope(DailyStatsFragment.class)
    DailyStatsPresenter provideDailyStatsPresenter(DailyStatsProvider provider, GeofencingClient client,
                                                   LocalUserCache cache, FitActivityTracker tracker, ScreenNavigator navigator,
                                                   UserTableApi userApi, Scheduler scheduler){
        return new DailyStatsPresenter(model, provider, client, cache, tracker, navigator, userApi, scheduler);
    }


    @Provides @FragmentScope(DailyStatsFragment.class)
    LiveUpdatesProvider provideLiveStatsProvider(){
        return new LiveUpdatesProvider(context);
    }

    @Provides @FragmentScope(DailyStatsFragment.class)
    LiveStatsPresenter provideLiveStatsPresenter(LiveUpdatesProvider provider){
        return new LiveStatsPresenter(model, provider);
    }
}
