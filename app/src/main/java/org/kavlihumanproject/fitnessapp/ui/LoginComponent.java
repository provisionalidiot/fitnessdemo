package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;

import dagger.Subcomponent;

@Subcomponent(modules = LoginScreenModule.class)
@ContextScope
public interface LoginComponent {

    void inject(LoginScreenActivity activity);

    @Subcomponent.Builder
    interface Builder {
        LoginComponent.Builder googlePlayServicesModule(GooglePlayServicesModule module);
        LoginComponent.Builder loginScreenModule(LoginScreenModule module);
        LoginComponent build();
    }
}
