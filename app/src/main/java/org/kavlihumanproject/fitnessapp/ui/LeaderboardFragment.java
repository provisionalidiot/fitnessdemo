package org.kavlihumanproject.fitnessapp.ui;
;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.databinding.FragmentLeaderboardBinding;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LeaderboardPresenter;
import org.kavlihumanproject.fitnessapp.ui.model.LeaderboardModel;

import javax.inject.Inject;

public class LeaderboardFragment extends Fragment {


    LeaderboardModel model = new LeaderboardModel();

    @Inject
    LeaderboardPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentLeaderboardBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_leaderboard, container, false);
        binding.setModel(model);
        return binding.getRoot();
    }

    @Override
    public void onStart(){
        super.onStart();
        Injector.injectLeaderboardFragment(this);
        presenter.getLeaderboard();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null){
            presenter.release();
        }
    }

    public LeaderboardModel getModel() {
        return model;
    }
}
