package org.kavlihumanproject.fitnessapp.ui;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.databinding.ActivityMainBinding;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.tasks.CalcStats;
import org.kavlihumanproject.fitnessapp.ui.model.MainScreenModel;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_NAME = "user_name";

    private MainScreenComponent component;


    @Override
    protected void onStart() {
        Injector.injectMainScreen(this);
        bindLayout();
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        component.getNavigator()
                .release();
    }

    public void bindLayout() {
        MainScreenModel model = new MainScreenModel(component.getUser());
        ActivityMainBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_main);
        binding.setModel(model);
    }

    public void calcCount(View v){
        CalcStats.all(this);
    }

    public static void launch(Context context, LocalUser activeUser) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(KEY_USER_NAME, activeUser.getUsername());
        intent.putExtra(KEY_USER_ID, activeUser.getId());
        context.startActivity(intent);
    }


    public void setComponent(MainScreenComponent component) {
        this.component = component;
    }

    public MainScreenComponent getComponent() {
        return component;
    }
}
