package org.kavlihumanproject.fitnessapp.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.databinding.ItemLeaderboardBinding;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;

import java.util.List;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.ViewHolder> {

    private final List<LocalUser> leaderboard;

    public LeaderboardAdapter(List<LocalUser> leaderboard) {
        this.leaderboard = leaderboard;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemLeaderboardBinding binding
                = DataBindingUtil.inflate(inflater,
                R.layout.item_leaderboard, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(leaderboard.get(position));
    }

    @Override
    public int getItemCount() {
        return leaderboard.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemLeaderboardBinding binding;

        ViewHolder(ItemLeaderboardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(LocalUser user){
            binding.setUser(user);
        }
    }
}
