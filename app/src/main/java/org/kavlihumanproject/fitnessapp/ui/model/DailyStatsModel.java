package org.kavlihumanproject.fitnessapp.ui.model;


import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.ActivityHistoryAdapter;

import java.util.List;

public class DailyStatsModel {

    public final ObservableBoolean showLoading = new ObservableBoolean();
    public final ObservableArrayList<DailyStat> items = new ObservableArrayList<>();
    public final ObservableInt errorCode = new ObservableInt();

    private final LocalUser user;

    public DailyStatsModel(LocalUser user){
        this.user = user;
    }

    public LocalUser getUser() {
        return user;
    }

    public void setShowLoading(boolean isLoading){
        showLoading.set(isLoading);
    }

    public void replaceStats(List<DailyStat> stats){
        items.clear();
        items.addAll(stats);
    }

    public void addStats(List<DailyStat> stats){
        items.addAll(stats);
    }

    @BindingAdapter({"app:items"})
    public static void setItems(RecyclerView dailyStats, ObservableArrayList<DailyStat> items) {
        ActivityHistoryAdapter adapter = new ActivityHistoryAdapter(items);
        dailyStats.setAdapter(adapter);
    }

    public void setErrorCode(int errorCode) {
        Log.e("error", "error code "+ errorCode);
        this.errorCode.set(errorCode);
    }
}
