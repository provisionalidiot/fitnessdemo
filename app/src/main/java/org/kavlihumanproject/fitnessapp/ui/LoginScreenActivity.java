package org.kavlihumanproject.fitnessapp.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.datamodel.LoginPresenter;
import org.kavlihumanproject.fitnessapp.ui.model.LoginScreenModel;

import javax.inject.Inject;

public class LoginScreenActivity extends AppCompatActivity {

    static final String BUNDLE_KEY_MODEL = "_model";

    @Inject
    LoginScreenModel model;

    @Inject
    LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle saved) {
        super.onCreate(saved);
        Injector.injectLoginPage(this, saved);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindViewModel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null){
            presenter.cancel();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BUNDLE_KEY_MODEL, model);
        super.onSaveInstanceState(outState);
    }

    private void bindViewModel(){
        org.kavlihumanproject.fitnessapp.databinding.ActivityLoginScreenBinding binding = DataBindingUtil.
                setContentView(this, R.layout.activity_login_screen);
        binding.setModel(model);
        binding.setPresenter(presenter);
    }

}
