package org.kavlihumanproject.fitnessapp.ui.model;

import android.databinding.BindingAdapter;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.ui.TabPagerAdapter;

public class MainScreenModel {

    public final LocalUser user;

    public MainScreenModel(LocalUser user) {
        this.user = user;
    }

    @BindingAdapter("app:pagerAdapter")
    public static void setPagerAdapter(ViewPager pager, LocalUser user) {
        AppCompatActivity activity = (AppCompatActivity) pager.getContext();
        FragmentManager manager = activity.getSupportFragmentManager();
        TabPagerAdapter adapter = new TabPagerAdapter(manager, user);
        pager.setAdapter(adapter);
        TabLayout tabs = (TabLayout) activity.findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);
    }
}
