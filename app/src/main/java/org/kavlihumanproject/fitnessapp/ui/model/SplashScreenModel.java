package org.kavlihumanproject.fitnessapp.ui.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;

public class SplashScreenModel {

    public final ObservableBoolean isLoading = new ObservableBoolean();
    public final ObservableInt errorCode = new ObservableInt();

    public void showLoading(boolean isLoading){
        this.isLoading.set(isLoading);
    }

    public void setErrorCode(int errorCode){
        this.errorCode.set(errorCode);
    }

}
