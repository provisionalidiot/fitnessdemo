package org.kavlihumanproject.fitnessapp.ui;

import org.kavlihumanproject.fitnessapp.dagger.FragmentScope;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;

import dagger.Subcomponent;

@Subcomponent(modules = DailyStatsModule.class)
@FragmentScope(DailyStatsFragment.class)
public interface DailyStatsComponent {

    void inject(DailyStatsFragment fragment);

    @Subcomponent.Builder
    interface Builder {
        DailyStatsComponent.Builder dailyStatsModule(DailyStatsModule module);
        DailyStatsComponent.Builder playServicesModule(GooglePlayServicesModule module);
        DailyStatsComponent build();
    }
}
