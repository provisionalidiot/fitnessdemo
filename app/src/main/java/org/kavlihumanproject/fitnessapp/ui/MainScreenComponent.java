package org.kavlihumanproject.fitnessapp.ui;


import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.ScreenNavigator;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;

import dagger.Subcomponent;

@ContextScope
@Subcomponent(modules = {MainScreenModule.class})
public interface MainScreenComponent {
    LocalUser getUser();
    ScreenNavigator getNavigator();
    DailyStatsComponent.Builder dailyStatsComponentBuilder();
    LeaderBoardComponent.Builder leaderboardComponentBuilder();

    @Subcomponent.Builder
    interface Builder {
        MainScreenComponent.Builder mainScreenModule(MainScreenModule module);
        MainScreenComponent build();
    }
}
