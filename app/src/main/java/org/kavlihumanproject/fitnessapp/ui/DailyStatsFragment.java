package org.kavlihumanproject.fitnessapp.ui;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GoogleApiAvailability;

import org.kavlihumanproject.fitnessapp.BR;
import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.databinding.FragmentDailyStatsBinding;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.impl.DailyStatsPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LiveStatsPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.User;
import org.kavlihumanproject.fitnessapp.tasks.CalcStats;
import org.kavlihumanproject.fitnessapp.ui.model.DailyStatsModel;

import javax.inject.Inject;

public class DailyStatsFragment extends Fragment {

    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";

    DailyStatsModel model;

    @Inject
    DailyStatsPresenter presenter;

    @Inject
    LiveStatsPresenter statsPresenter;


    private boolean activityIsVisible;

    public DailyStatsFragment(){
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDailyStatsBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_daily_stats, container, false);
        bindLayout(binding);
        binding.notifyPropertyChanged(BR.item);
        return binding.getRoot();
    }

    private void bindLayout(FragmentDailyStatsBinding binding){
        binding.setModel(model);
        model.errorCode.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int id) {
                int errorCode = model.errorCode.get();
                onGooglePlayError(errorCode);
            }
        });
    }

    public void onGooglePlayError(int errorCode) {
        if (activityIsVisible) {
            GoogleApiAvailability
                    .getInstance()
                    .showErrorDialogFragment(getActivity(), errorCode, 11);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        activityIsVisible = true;
        Injector.injectDailyStatsFragment(this);
        statsPresenter.listenForLiveUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getRecentStats();
    }

    @Override
    public void onStop() {
        super.onStop();
        activityIsVisible = false;
        if (presenter != null) {
            presenter.release();
        }

        if (statsPresenter != null){
            statsPresenter.release();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu.findItem(R.id.logout) == null) {
            inflater.inflate(R.menu.options, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.logout:
                presenter.logout();
                return true;
            case R.id.office:
                presenter.setLocationAsOffice();
                return true;
            case R.id.calcStats:
                long userId = model.getUser().getId();
                CalcStats.userStats(getContext(), userId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void buildViewModel(){
        LocalUser user = buildUser();
        model = new DailyStatsModel(user);
    }

    private LocalUser buildUser() {
        Bundle args = getArguments();
        long id = args.getLong(KEY_USER_ID, -1);
        String username = args.getString(KEY_USER_NAME);
        return new User(id, username);
    }

    public static DailyStatsFragment newInstance(LocalUser user){
        Bundle args = new Bundle();
        args.putLong(KEY_USER_ID, user.getId());
        args.putString(KEY_USER_NAME, user.getUsername());
        DailyStatsFragment fragment = new DailyStatsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public DailyStatsModel getModel() {
        return model;
    }
}
