package org.kavlihumanproject.fitnessapp.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.databinding.ItemDailyStatisticBinding;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;

import java.util.List;


public class ActivityHistoryAdapter extends RecyclerView.Adapter<ActivityHistoryAdapter.ViewHolder> {

    private final List<DailyStat> items;

    public ActivityHistoryAdapter(List<DailyStat> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemDailyStatisticBinding binding
                = DataBindingUtil.inflate(inflater,
                    R.layout.item_daily_statistic, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DailyStat stat = items.get(position);
        holder.bind(stat);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemDailyStatisticBinding binding;
        ViewHolder(ItemDailyStatisticBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(DailyStat item){
            binding.setItem(item);
            binding.executePendingBindings();
        }
    }
}
