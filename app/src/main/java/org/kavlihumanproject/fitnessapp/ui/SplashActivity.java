package org.kavlihumanproject.fitnessapp.ui;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.GoogleApiAvailability;

import org.kavlihumanproject.fitnessapp.R;
import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.databinding.ActivitySplashBinding;
import org.kavlihumanproject.fitnessapp.datamodel.PermissionHelper;
import org.kavlihumanproject.fitnessapp.datamodel.SplashPresenter;
import org.kavlihumanproject.fitnessapp.ui.model.SplashScreenModel;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {

    @Inject
    SplashPresenter presenter;

    @Inject
    PermissionHelper permissionHelper;

    @Inject
    SplashScreenModel model;

    private boolean activityIsVisible;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViewModel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityIsVisible = true;
        Injector.injectSplashActivity(this);
        presenter.checkForActiveUser();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        activityIsVisible = false;
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null){
            presenter.cancel();
        }
    }

    private void bindViewModel(){
        this.model = new SplashScreenModel();
        ActivitySplashBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_splash);

        model.errorCode.addOnPropertyChangedCallback(
                new Observable.OnPropertyChangedCallback() {
                    @Override
                    public void onPropertyChanged(Observable observable, int error) {
                        onGooglePlayError(error);
                    }
                }
        );

        binding.setModel(model);
    }

    public void onGooglePlayError(int errorCode) {
        if (activityIsVisible) {
            GoogleApiAvailability
                    .getInstance()
                    .showErrorDialogFragment(this, errorCode, 11);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (permissionHelper != null
                && !permissionHelper.isReleased()) {
            permissionHelper.handleRequestResults(
                    requestCode, permissions, grantResults);
        }
    }
}
