package org.kavlihumanproject.fitnessapp.ui.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import org.kavlihumanproject.fitnessapp.R;

public class LoginScreenModel implements Parcelable {

    public final ObservableField<String> username;
    public final ObservableField<String> password;
    public final ObservableField<String> confirm;

    public final ObservableBoolean showConfirm;
    public final ObservableBoolean showLoading;
    public final ObservableInt errorMessage;

    public LoginScreenModel(){
        username = new ObservableField<>();
        password = new ObservableField<>();
        confirm = new ObservableField<>();
        showConfirm = new ObservableBoolean();
        showLoading = new ObservableBoolean();
        errorMessage = new ObservableInt(R.string.empty_space); // initialize with R.string.empty_space
                                                                // to avoid NoResourceFoundException
                                                                // when binding without an error message
    }

    protected LoginScreenModel(Parcel in) {
        username = new ObservableField<>(in.readString());
        password = new ObservableField<>(in.readString());
        confirm = new ObservableField<>(in.readString());
        showConfirm = new ObservableBoolean(in.readInt() == 1);
        showLoading = new ObservableBoolean(in.readInt() == 1);
        errorMessage = new ObservableInt(in.readInt());
    }

    public static final Creator<LoginScreenModel> CREATOR = new Creator<LoginScreenModel>() {
        @Override
        public LoginScreenModel createFromParcel(Parcel in) {
            return new LoginScreenModel(in);
        }

        @Override
        public LoginScreenModel[] newArray(int size) {
            return new LoginScreenModel[size];
        }
    };

    public String getUserName() {
        String username =  this.username.get();
        return username != null ? username : "";
    }

    public String getPasswd() {
        String password = this.password.get();
        return password != null ? password : "";
    }

    public String getConfrm() {
        String confirm = this.confirm.get();
        return confirm != null ? confirm : "";
    }

    public void setShowLoading(boolean showLoading) {
        this.showLoading.set(showLoading);
    }

    public void showErrorMessage(@StringRes int errorMessage){
        this.errorMessage.set(errorMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUserName());
        parcel.writeString(getPasswd());
        parcel.writeString(confirm.get());
        parcel.writeInt(showConfirm.get() ? 1 : 0);
        parcel.writeInt(showLoading.get() ? 1 : 0);
        parcel.writeInt(errorMessage.get());
    }

    public void setShowConfirm(boolean showConfirm) {
        this.showConfirm.set(showConfirm);
    }

    public void clearConfirm(){
        this.confirm.set("");
    }
}
