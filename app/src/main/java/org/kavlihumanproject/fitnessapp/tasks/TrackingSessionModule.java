package org.kavlihumanproject.fitnessapp.tasks;

import org.kavlihumanproject.fitnessapp.dagger.SessionScope;
import org.kavlihumanproject.fitnessapp.datamodel.LocationTracker;
import org.kavlihumanproject.fitnessapp.datamodel.SessionRecorder;
import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;
import org.kavlihumanproject.fitnessapp.datamodel.impl.SessionRecorderImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.TrackingSessionImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DailyStatsApi;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.SessionDataApi;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public class TrackingSessionModule {

    private final long userId;

    public TrackingSessionModule(long userId) {
        this.userId = userId;
    }

    @Provides
    @SessionScope
    TrackingSession provideTrackingSession(LocationTracker tracker, SessionRecorder recorder){
        return new TrackingSessionImpl(userId, tracker, recorder);
    }

    @Provides
    @SessionScope
    public SessionRecorder provideRecorder(SessionDataApi sessionApi, DailyStatsApi statsApi, Scheduler scheduler){
        return new SessionRecorderImpl(sessionApi, statsApi, scheduler);
    }
}
