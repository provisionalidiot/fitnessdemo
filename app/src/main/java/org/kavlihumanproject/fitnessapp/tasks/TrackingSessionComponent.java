package org.kavlihumanproject.fitnessapp.tasks;


import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;

import dagger.Subcomponent;

@Subcomponent(modules = TrackingSessionModule.class)
public interface TrackingSessionComponent {
    TrackingSession getSession();

    @Subcomponent.Builder
    interface Builder {
        TrackingSessionComponent.Builder sessionModule(TrackingSessionModule module);
        TrackingSessionComponent build();
    }
}
