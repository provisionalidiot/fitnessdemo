package org.kavlihumanproject.fitnessapp.tasks;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;

import dagger.Subcomponent;

@Subcomponent(modules = TrackerServiceModule.class)
@ContextScope
public interface TrackerServiceComponent {
    void inject(ActivityTrackerService service);
    FitActivityTracker getFitTracker();
    TrackingSessionComponent.Builder sessionComponentBuilder();
    @Subcomponent.Builder
    interface Builder {
        TrackerServiceComponent.Builder playServicesModule(GooglePlayServicesModule module);
        TrackerServiceComponent.Builder trackerServiceModule(TrackerServiceModule module);
        TrackerServiceComponent build();
    }
}
