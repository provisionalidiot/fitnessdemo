package org.kavlihumanproject.fitnessapp.tasks;

import org.kavlihumanproject.fitnessapp.datamodel.data.ActivitySession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;

public class LiveStat implements DailyStat {

    private final long dailyStatId;
    private final double distance;
    private final double duration;
    private final int session;

    public LiveStat(long dailyStatId, double distance, double duration, int session) {
        this.dailyStatId = dailyStatId;
        this.distance = distance;
        this.duration = duration;
        this.session = session;
    }

    @Override
    public long getId() {
        return dailyStatId;
    }

    @Override
    public String getDate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getSessions() {
        return session;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public double getDuration() {
        return duration;
    }

    @Override
    public DailyStat plus(ActivitySession session) {
        throw new UnsupportedOperationException();
    }
}
