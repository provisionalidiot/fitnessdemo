package org.kavlihumanproject.fitnessapp.tasks;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.kavlihumanproject.fitnessapp.dagger.Injector;
import org.kavlihumanproject.fitnessapp.datamodel.FitActivityTracker;
import org.kavlihumanproject.fitnessapp.datamodel.NotificationPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.impl.ActivityFilter;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subjects.PublishSubject;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class ActivityTrackerService extends Service implements Observer<Double> {

    private static final String ACTION_ACTIVITY_RECOGNIZED
            = "ActivityTrackerService.ACTIVITY_RECOGNIZED";

    private static final String ACTION_FORCE_STOP
            = "ActivityTrackerService.FORCE_STOP_SESSION";
    public static final String EXTRA_USER_ID
            = "ActivityTrackingService.user_id";

    private final LiveStatsBinder BINDER = new LiveStatsBinder();

    @Inject
    NotificationPresenter notifier;

    @Inject
    TrackerServiceComponent component;

    TrackingSession session;


    private Subscription subscription;

    public static void stopSession(@NonNull Context context){
        Intent stop = new Intent(context, ActivityTrackerService.class);
        stop.setAction(ACTION_FORCE_STOP);
        context.startService(stop);
    }

    public static PendingIntent buildIntent(long userId, Context context){
        Intent activityTracking = new Intent(context, ActivityTrackerService.class);
        activityTracking.setAction(ACTION_ACTIVITY_RECOGNIZED);
        activityTracking.putExtra(EXTRA_USER_ID, userId);
        return PendingIntent
                .getService(context, 0, activityTracking, FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.injectTrackingSevice(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private TrackingSession buildSession(long userId){
        return TrackingSessionBuilder
                .build(userId, component);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return BINDER;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (session == null){
            long userId = intent.getLongExtra(EXTRA_USER_ID, -1);
            if (userId > -1) {
                session = buildSession(userId);
            }else {
                stopTrackingActivities();
                stopSelf();
                return START_NOT_STICKY;
            }
        }

        if (ActivityFilter.isTrackingIntent(intent)) {
                handleIntent(intent);
        }else if (intent != null && ACTION_FORCE_STOP.equals(intent.getAction())){
            if (session.isActive()){
                session.endSession();
                session = null;
            }
        }else  if (subscription == null || subscription.isUnsubscribed()) {
            stopSelfResult(startId);
        }

        return START_REDELIVER_INTENT;
    }

    private void handleIntent(Intent intent){
        ActivityFilter filter
                = ActivityFilter.from(intent);

        if (session.isActive() && filter.shouldStop()){
            session.endSession();
            return;
        }

        if (!session.isActive()){
            if (filter.shouldStart()){
                subscription = session.startSession()
                        .subscribe(this);
            }
        }
    }

    @Override
    public void onCompleted() {
        subscription.unsubscribe();
        CalcStats.session(this, session.getSessionId());   /* Proccess the collected data and update current values */
        CalcStats.dailyStat(this, session.getUserId(),
                TimeUtil.getToday());
        BINDER.updateSessionEnded();
        session = null;
        stopSelf();
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        BINDER.onError(e);
        notifier.showReminderNotificaion();
        if (session.isActive()){
            session.endSession();
        }
        stopSelf();
    }

    @Override
    public void onNext(Double distance) {
        if (session.didReachMilestone()) {
            int milestone = session.getCurrentMilestone();
            notifier.showMilestoneNotification(milestone);
        }
        BINDER.updateStats();
    }

    void stopTrackingActivities(){
        Log.e("ActivityTrackerService", "No Active user! Shutting down");
        FitActivityTracker fitTracker = component.getFitTracker();
        fitTracker.stopListening()
                .subscribe((b) -> {
                   fitTracker.release();
                    stopSelf();
                }, this::onError);
    }

    public static Observable<DailyStat> listenForUpdates(Context context){
        LiveStatsConnection connection = new LiveStatsConnection(context);
        return connection.liveUpdates();
    }
    
    class LiveStatsBinder extends Binder {
        private final PublishSubject<DailyStat> liveUpdate;
        private LiveStatsBinder() {
            liveUpdate = PublishSubject.create();
        }
        
        Observable<DailyStat> listenForUpdates(){
            return liveUpdate;
        }
        
        void updateStats(){
            if (session.isActive() && !liveUpdate.hasCompleted()){
                liveUpdate.onNext(session.getUpdatedStats());
            }
        }
        
        void updateSessionEnded(){
            if (!liveUpdate.hasCompleted()) {
                liveUpdate.onCompleted();
            }
        }
        
        void onError(Throwable t){
            if (!liveUpdate.hasCompleted()){
                liveUpdate.onError(t);
            }
        }

        TrackingSession getSession(){
            return session;
        }

        @Override
        public String toString() {
            return "Binder session is " + (session != null && session.isActive() ? "active" : "not active");
        }
    }
    
}
