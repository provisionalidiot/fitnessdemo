package org.kavlihumanproject.fitnessapp.tasks;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.GeofencingEvent;

import org.kavlihumanproject.fitnessapp.datamodel.NotificationPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.NotificationPresenterImpl;

import static android.app.AlarmManager.INTERVAL_HOUR;
import static android.app.PendingIntent.FLAG_NO_CREATE;
import static com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_DWELL;
import static org.kavlihumanproject.fitnessapp.datamodel.GeofencingClient.GEOFENCE_TRIGGER_ACTION;

public class ReminderService extends IntentService {

    private static final String SERVICE_NAME = "GEOFENCE_REMINDER_SERVICE";
    private static final String ACTION_ALARM = "org.kavlihumanproject.fitnessapp.tasks.ReminderService.ACTION_ALARM";

    AlarmManager manager;
    NotificationPresenter notifier;

    public ReminderService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        buildDependencies();
        super.onCreate();
    }

    void buildDependencies(){
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        notifier = buildNotifView();
    }

    private NotificationPresenterImpl buildNotifView(){
        NotificationManager manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        return new NotificationPresenterImpl(this, manager);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null){
            return;
        }

        switch (intent.getAction()){
            case GEOFENCE_TRIGGER_ACTION:
                handleGeofenceTrigger(intent);
                break;
            case ACTION_ALARM:
                notifier.showReminderNotificaion();
                break;
            default:
                Log.e("ReminderService", "Unknown Action: " + intent.getAction());
                cancelAlarm(this);
                break;
        }
    }

    private void setAlarm() {
        if (!checkIfAlarmed()) {
            PendingIntent alarmIntent = buildAlarmIntent();
            manager.setRepeating(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis(), INTERVAL_HOUR, alarmIntent);
        }
    }

    private void handleGeofenceTrigger(Intent intent){
        GeofencingEvent event
                = GeofencingEvent.fromIntent(intent);
        if (event.getGeofenceTransition() == GEOFENCE_TRANSITION_DWELL){
            setAlarm();
        }else {
            cancelAlarm(this);
        }
    }

    private void sendMessage(String message){
        ((NotificationPresenterImpl)notifier).showMessage(message);
    }

    private boolean checkIfAlarmed(){
        Intent startService = new Intent(this, ReminderService.class);
        startService.setAction(ACTION_ALARM);
        return PendingIntent
                .getService(this, 0, startService, FLAG_NO_CREATE) != null;
    }

    private  PendingIntent buildAlarmIntent(){
        Intent startService = new Intent(this, ReminderService.class);
        startService.setAction(ACTION_ALARM);
        return PendingIntent
                .getService(this, 0, startService, 0);
    }

    public static void cancelAlarm(Context context){
        Intent startService = new Intent(context, ReminderService.class);
        startService.setAction(ACTION_ALARM);
        PendingIntent pendingIntent =  PendingIntent
                .getService(context, 0, startService, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

}
