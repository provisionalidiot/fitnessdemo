package org.kavlihumanproject.fitnessapp.tasks;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.kavlihumanproject.fitnessapp.datamodel.LocalUserProvider;
import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.datamodel.data.LocalUser;
import org.kavlihumanproject.fitnessapp.datamodel.impl.DailyStatsProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.LocalUserDbProviderImpl;
import org.kavlihumanproject.fitnessapp.datamodel.impl.SessionDataProvider;
import org.kavlihumanproject.fitnessapp.datamodel.impl.database.DbHelper;
import org.kavlihumanproject.fitnessapp.util.TimeUtil;

import rx.Observable;

public class CalcStats extends IntentService {
    private static final String NAME = "CalcStats";
    private static final long INVALID = -1;

    private static final String ACTION_ALL = "org.kavlihumanproject.fitnessapp.tasks.ACTION_ALL_STATS";
    private static final String ACTION_SESSION = "org.kavlihumanproject.fitnessapp.tasks.ACTION_SESSION";
    private static final String ACTION_USER_STATS = "org.kavlihumanproject.fitnessapp.tasks.ACTION_USER_STATS";
    private static final String ACTION_DAILY_STAT = "org.kavlihumanproject.fitnessapp.tasks.DAILY_STAT";

    private static final String EXTRA_SESSION = "org.kavlihumanproject.fitnessapp.tasks.EXTRA_SESSION";
    private static final String EXTRA_USER_ID = "org.kavlihumanproject.fitnessapp.tasks.EXTRA_USER_ID";
    private static final String EXTRA_DATE = "org.kavlihumanproject.fitnessapp.tasks.EXTRA_DATE";


    private DbHelper helper;
    private DailyStatsProvider dailyStatsProvider;
    private SessionDataProvider sessionDataProvider;
    private LocalUserProvider userProvider;

    public CalcStats() {
        super(NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        helper = DbHelper.getHelper(this);
        sessionDataProvider = new SessionDataProvider(helper.getSessionDataApi(), helper.getScheduler());
        dailyStatsProvider = new DailyStatsProvider(helper.getDailyStatsApi(), sessionDataProvider, helper.getScheduler());
        userProvider = new LocalUserDbProviderImpl(helper.getUserTableApi(), helper.getDailyStatsApi(), helper.getScheduler());
    }

    public static void all(Context context){
        Intent sessionIntent = new Intent(context, CalcStats.class);
        sessionIntent.setAction(ACTION_ALL);
        context.startService(sessionIntent);
    }

    public static void userStats(Context context, long userId){
        Intent sessionIntent = new Intent(context, CalcStats.class);
        sessionIntent.setAction(ACTION_USER_STATS);
        sessionIntent.putExtra(EXTRA_USER_ID, userId);
        context.startService(sessionIntent);
    }

    public static void dailyStat(Context context, long userId, long date){
        Intent dailyStatIntent = new Intent(context, CalcStats.class);
        dailyStatIntent.setAction(ACTION_DAILY_STAT);
        dailyStatIntent.putExtra(EXTRA_USER_ID, userId);
        dailyStatIntent.putExtra(EXTRA_DATE, date);
        context.startService(dailyStatIntent);
    }

    public static void session(Context context, long sessionId){
        Intent sessionIntent = new Intent(context, CalcStats.class);
        sessionIntent.setAction(ACTION_SESSION);
        sessionIntent.putExtra(EXTRA_SESSION, sessionId);
        context.startService(sessionIntent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null){
            return;
        }

        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        if (action == null || (extras == null && !ACTION_ALL.equals(action))){
            throw new AssertionError("Intent action or data is missing");
        }
        switch (action) {
            case ACTION_SESSION: {
                long session = extras.getLong(EXTRA_SESSION, INVALID);
                if (session != INVALID) {
                    calcSession(session);
                }
                break;
            }
            case ACTION_DAILY_STAT: {
                long userId = extras.getLong(EXTRA_USER_ID, INVALID);
                long date = extras.getLong(EXTRA_DATE, INVALID);
                if (userId != INVALID && date != INVALID) {
                    calcDailyStat(userId, date);
                }
                break;
            }
            case ACTION_USER_STATS: {
                long userId = extras.getLong(EXTRA_USER_ID, INVALID);
                if (userId != INVALID) {
                    calcUserStats(userId);
                }
                break;
            }
            case ACTION_ALL: {
                calcAllStats();
                break;
            }
            default: {
                throw new AssertionError("Unknown action: " + action);
            }
        }
    }

    private void calcSession(long sessionId) {
        Log.e("CalcStats", "calclating session "+sessionId);
        sessionDataProvider
                .getSessionData(sessionId)
                .subscribe(session -> {
                    Log.e("CalcStats", "Session data: " + session + "\n\n");
                }, this::handleError);
    }

    private void calcDailyStat(long userId, long date){
        Log.e("CalcStats", "calculating dailystat " + userId + " " + TimeUtil.printDay(date));
        dailyStatsProvider
                .calcDailyStat(userId, date)
                .subscribe(stat -> {
                    Log.e("CalcStats", "Daily stat: " + stat + "\n\n");
                }, this::handleError);
    }

    private void calcUserStats(long userId){
        Log.e("CalcStats", "calculating stats for user "+userId);
        dailyStatsProvider
                .recalculateDailyStats(userId, System.currentTimeMillis() - 48 * 60 * 60 * 1000L)
                .subscribe(stats -> {
                    for (DailyStat stat : stats) {
                        Log.e("CalcStats", "Daily stat : " + stat + "\n\n");
                    }
                }, this::handleError);
    }


    private void calcAllStats() {
        Log.e("CalcStats","calculate all stats leaderboard");

     Observable<Long> allUsersIds = userProvider
                .getAllUsers()
                .map(LocalUser::getId)
                .cache();

        allUsersIds.flatMap(sessionDataProvider::getAllSessionIds)
                .subscribeOn(helper.getScheduler())
                .doOnNext(this::calcSession)
                .toBlocking()
                .subscribe();

        allUsersIds.doOnNext(this::calcUserStats)
                .toBlocking()
                .subscribe();

        userProvider.calculateLeaderBoard()
                .toBlocking()
                .subscribe();
    }


    void handleError(Throwable t){
        Log.e("CalcStats", "Encountered and error!");
        t.printStackTrace();
    }

}
