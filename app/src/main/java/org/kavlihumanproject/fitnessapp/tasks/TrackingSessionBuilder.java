package org.kavlihumanproject.fitnessapp.tasks;

import org.kavlihumanproject.fitnessapp.datamodel.TrackingSession;
import org.kavlihumanproject.fitnessapp.util.testing.TestingUtil;

public class TrackingSessionBuilder {
    private static boolean isTest;

    public static void setIsTest(boolean isTest) {
        TrackingSessionBuilder.isTest = isTest;
    }

    static TrackingSession build(long userId, TrackerServiceComponent component){
        if (isTest){
            return TestingUtil
                    .createDelayedTrackingSession();
        }

        return component.sessionComponentBuilder()
                .sessionModule(new TrackingSessionModule(userId))
                .build()
                .getSession();
    }
}
