package org.kavlihumanproject.fitnessapp.tasks;

import android.app.NotificationManager;
import android.content.Context;

import org.kavlihumanproject.fitnessapp.dagger.ContextScope;
import org.kavlihumanproject.fitnessapp.datamodel.NotificationPresenter;
import org.kavlihumanproject.fitnessapp.datamodel.impl.GooglePlayServicesModule;
import org.kavlihumanproject.fitnessapp.datamodel.impl.NotificationPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module(
        subcomponents = TrackingSessionComponent.class,
        includes = GooglePlayServicesModule.class
)
public class TrackerServiceModule {

    private final ActivityTrackerService service;

    public TrackerServiceModule(ActivityTrackerService service) {
        this.service = service;
    }

    @Provides @ContextScope
    NotificationManager provideNotificationManager(){
        return (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Provides @ContextScope
    NotificationPresenter provideNotifier(NotificationManager manager){
        return new NotificationPresenterImpl(service, manager);
    }

}
