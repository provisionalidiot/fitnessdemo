package org.kavlihumanproject.fitnessapp.tasks;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import org.kavlihumanproject.fitnessapp.datamodel.data.DailyStat;
import org.kavlihumanproject.fitnessapp.util.RxLogger;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class LiveStatsConnection implements ServiceConnection, Observable.OnSubscribe<ActivityTrackerService.LiveStatsBinder> {
    private final Context context;
    private Subscriber<? super ActivityTrackerService.LiveStatsBinder> subscriber;
    LiveStatsConnection(Context context) {
        this.context = context;
    }

    @Override
    public synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ActivityTrackerService.LiveStatsBinder binder = (ActivityTrackerService.LiveStatsBinder) iBinder;
        if (subscriber != null && !subscriber.isUnsubscribed()){
            subscriber.onNext(binder);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (subscriber != null && !subscriber.isUnsubscribed()){
            subscriber.onError(new RuntimeException("service disconnected"));
        }
    }

    private void unbind(){
        context.unbindService(this);
    }

    Observable<DailyStat> liveUpdates(){
        return Observable.create(this)
                .map(new RxLogger<>("Binder"))
                .flatMap(ActivityTrackerService.LiveStatsBinder::listenForUpdates)
                .subscribeOn(Schedulers.computation())
                .doOnUnsubscribe(this::unbind)
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void call(Subscriber<? super ActivityTrackerService.LiveStatsBinder> subscriber) {
        this.subscriber = subscriber;
        Intent bind = new Intent(context, ActivityTrackerService.class);
        context.bindService(bind, this, Context.BIND_AUTO_CREATE);
    }
}
